#!/usr/bin/env bash

echo "> run norovirus test"

# Script setup to test new feature of EGG software:
# --include-regexp and --exclude-regexp arguments.

########################################
##      Setup working directory       ## 
########################################
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory
if [  ! "$SCRATCH"  ]; then
  SCRATCH=/tmp
fi
# Caution: always use a defined working directory:
#          it'll be deleted by the end of the script!
if [  ! "$CB_LOG_FOLDER"  ]; then
  CB_LOG_FOLDER=$SCRATCH/extractGene
fi
mkdir -p "$CB_LOG_FOLDER"

# TMP is used by Python script to create there a log file
export TMP=$CB_LOG_FOLDER

########################################
##      Load Biopython environment    ##
########################################
BIOPYTHON_ENV="/appli/bioinfo/biopython/1.78/env.sh"
if [ -f "$BIOPYTHON_ENV" ]; then
  . $BIOPYTHON_ENV
fi

########################################
##      Configure script for test 1   ##
########################################

# This test illustrates this feature of EGG: if a genbank entry contains
# $FEAT_FILTER matching $INCLUDE_RE, then if that match also matches against
# $EXCLUDE_RE, then that feature is actually discarded. It enables to keep
# all "capsid protein" but "minor" or "vp2" ones.

FNAME="noro-orf2"
INCLUDE_RE="(orf2|vp1|major.?capsid|capsid.?protein)"
EXCLUDE_RE="minor,vp2"
FEAT_FILTER="CDS:gene|note|product"
TYPE="genbank"
GB_FILE="$SCRIPT_DIR/orf2-reference.gb"
GB_FILE_REF="$SCRIPT_DIR/orf2-reference-out.gb"
OUT_FILE="$CB_LOG_FOLDER/${FNAME}.gb"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --include-regexp "$INCLUDE_RE" \
  --exclude-regexp "$EXCLUDE_RE" \
  --gb-file        "$GB_FILE" \
  --feature-filter "$FEAT_FILTER" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_FILE_REF > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 1: OK"

########################################
##      Configure script for test 2   ##
########################################

ADDON_DATA_FILE="$SCRIPT_DIR/orf2-reference.csv"
GB_FILE_REF="$SCRIPT_DIR/orf2-reference-out2.gb"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --include-regexp "$INCLUDE_RE" \
  --exclude-regexp "$EXCLUDE_RE" \
  --gb-file        "$GB_FILE" \
  --feature-filter "$FEAT_FILTER" \
  --data-file      "$ADDON_DATA_FILE" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_FILE_REF > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 2: OK"

########################################
##      Configure script for test 3   ##
########################################
TYPE="simple" # aka FASTA
DATA_DESC_MODE="after"
GB_FILE_REF="$SCRIPT_DIR/orf2-reference-out3.fas"
OUT_FILE="$CB_LOG_FOLDER/${FNAME}.fas"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --include-regexp "$INCLUDE_RE" \
  --exclude-regexp "$EXCLUDE_RE" \
  --gb-file        "$GB_FILE" \
  --feature-filter "$FEAT_FILTER" \
  --data-file      "$ADDON_DATA_FILE" \
  --data-desc-mode "$DATA_DESC_MODE" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_FILE_REF > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 3: OK"