#!/usr/bin/env bash

echo "> run misc test"

########################################
##      Setup working directory       ## 
########################################
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory
if [  ! "$SCRATCH"  ]; then
  SCRATCH=/tmp
fi
# Caution: always use a defined working directory:
#          it'll be deleted by the end of the script!
if [  ! "$CB_LOG_FOLDER"  ]; then
  CB_LOG_FOLDER=$SCRATCH/extractGene
fi
mkdir -p "$CB_LOG_FOLDER"

# TMP is used by Python script to create there a log file
export TMP=$CB_LOG_FOLDER

########################################
##      Load Biopython environment    ##
########################################
BIOPYTHON_ENV="/appli/bioinfo/biopython/1.78/env.sh"
if [ -f "$BIOPYTHON_ENV" ]; then
  . $BIOPYTHON_ENV
fi

########################################
##      Configure script for test 1   ##
########################################

# This test illustrates this feature of EGG: if a same genbank entry contains
# several times the same feture (here, misc_feature), then all these features
# are used to create as many new entries. NC_005816.gb contains 13 misc_feature,
# then, resulting EGG processed file is a single Genbank file with 13 entries.

GENESNAMES="YP_p"
FEAT_FILTER="misc_feature:locus_tag"
TYPE="genbank"
GB_FILE="$SCRIPT_DIR/NC_005816.gb"
GB_FILE_REF="$SCRIPT_DIR/NC_005816-reference.gb"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}.gb"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --genes-names    "$GENESNAMES" \
  --gb-file        "$GB_FILE" \
  --feature-filter "$FEAT_FILTER" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test 1 has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_FILE_REF > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test 1 has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 1: OK"

########################################
##      Configure script for test 2   ##
########################################
GENESNAMES="glucuronidase"
FEAT_FILTER="CDS:note"
TYPE="genbank"
GB_FILE="$SCRIPT_DIR/RATGLCB.gb"
GB_FILE_REF="$SCRIPT_DIR/RATGLCB-reference.gb"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}.gb"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --genes-names    "$GENESNAMES" \
  --gb-file        "$GB_FILE" \
  --feature-filter "$FEAT_FILTER" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test 2 has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_FILE_REF > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test 2 has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 2: OK"

########################################
##      Configure script for test 3   ##
########################################
TYPE="beedeem"

GENESNAMES="12S"
FEAT_FILTER="rRNA:gene|product"
TAXO_FILTER_INC="Anatololacerta"
GB_FILE="$SCRIPT_DIR/gbvrt56-reference.gb"
GB_REF_FILE="$SCRIPT_DIR/gbvrt56-reference-bdm.gb"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}.gb"

$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --genes-names    "$GENESNAMES" \
  --gb-file        "$GB_FILE" \
  --feature-filter "$FEAT_FILTER" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test 3 has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_REF_FILE > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test 3 has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 3: OK"