
LOCUS       CVXRNA                  7654 bp ss-RNA     linear   VRL 01-JUN-2006
DEFINITION  Norwalk virus nonstructural polyprotein, 58 kd capsid protein, and
            orf3 genes, complete cds.
ACCESSION   M87661
VERSION     M87661.2
KEYWORDS    .
SOURCE      Norwalk virus
  ORGANISM  Norwalk virus
            Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes;
            Picornavirales; Caliciviridae; Norovirus.
REFERENCE   1  (bases 13 to 7654)
  AUTHORS   Jiang,X., Wang,M., Wang,K. and Estes,M.K.
  TITLE     Sequence and genomic organization of Norwalk virus
  JOURNAL   Virology 195 (1), 51-61 (1993)
   PUBMED   8391187
REFERENCE   2  (bases 1 to 12)
  AUTHORS   Hardy,M.E. and Estes,M.K.
  TITLE     Completion of the Norwalk virus genome sequence
  JOURNAL   Virus Genes 12 (3), 287-290 (1996)
   PUBMED   8883366
REFERENCE   3  (bases 13 to 7654)
  AUTHORS   Estes,M.K.
  TITLE     Direct Submission
  JOURNAL   Submitted (09-AUG-1993) Molecular Virology, Baylor College of
            Medicine, Houston, TX 77006, USA
REFERENCE   4  (bases 1 to 12)
  AUTHORS   Hardy,M.E.
  TITLE     Direct Submission
  JOURNAL   Submitted (07-NOV-1995) Molecular Virology, Baylor College of
            Medicine, Houston, TX 77006, USA
REFERENCE   5  (bases 1 to 7654)
  AUTHORS   Estes,M.K.
  TITLE     Direct Submission
  JOURNAL   Submitted (01-JUN-2006) Department of Molecular Virology and
            Microbiology, Baylor College of Medicine, One Baylor Plaza,
            Houston, TX 77030, USA
  REMARK    Sequence update by submitter
COMMENT     On Jun 1, 2006 this sequence version replaced M87661.1.
FEATURES             Location/Qualifiers
     source          1..7654
                     /organism="Norwalk virus"
                     /mol_type="genomic RNA"
                     /db_xref="taxon:11983"
     CDS             5..5374
                     /note="orf1; sequence homologies to 2C helicase, 3C
                     protease, and 3D RNA-dependent RNA polymerase of
                     picornavirus"
                     /codon_start=1
                     /product="nonstructural polyprotein"
                     /protein_id="AAB50465.1"
                     /translation="MMMASKDVVPTAASSENANNNSSIKSRLLARLKGSGGATSPPNS
                     IKITNQDMALGLIGQVPAPKATSVDVPKQQRDRPPRTVAEVQQNLRWTERPQDQNVKT
                     WDELDHTTKQQILDEHAEWFDAGGLGPSTLPTSHERYTHENDEGHQVKWSAREGVDLG
                     ISGLTTVSGPEWNMCPLPPVDQRSTTPATEPTIGDMIEFYEGHIYHYAIYIGQGKTVG
                     VHSPQAAFSITRITIQPISAWWRVCYVPQPKQRLTYDQLKELENEPWPYAAVTNNCFE
                     FCCQVMCLEDTWLQRKLISSGRFYHPTQDWSRDTPEFQQDSKLEMVRDAVLAAINGLV
                     SRPFKDLLGKLKPLNVLNLLSNCDWTFMGVVEMVVLLLELFGIFWNPPDVSNFIASLL
                     PDFHLQGPEDLARDLVPIVLGGIGLAIGFTRDKVSKMMKNAVDGLRAATQLGQYGLEI
                     FSLLKKYFFGGDQTEKTLKDIESAVIDMEVLSSTSVTQLVRDKQSARAYMAILDNEEE
                     KARKLSVRNADPHVVSSTNALISRISMARAALAKAQAEMTSRMRPVVIMMCGPPGIGK
                     TKAAEHLAKRLANEIRPGGKVGLVPREAVDHWDGYHGEEVMLWDDYGMTKIQEDCNKL
                     QAIADSAPLTLNCDRIENKGMQFVSDAIVITTNAPGPAPVDFVNLGPVCRRVDFLVYC
                     TAPEVEHTRKVSPGDTTALKDCFKPDFSHLKMELAPQGGFDNQGNTPFGKGVMKPTTI
                     NRLLIQAVALTMERQDEFQLQGPTYDFDTDRVAAFTRMARANGLGLISMASLGKKLRS
                     VTTIEGLKNALSGYKISKCSIQWQSRVYIIESDGASVQIKEDKQALTPLQQTINTASL
                     AITRLKAARAVAYASCFQSAITTILQMAGSALVINRAVKRMFGTRTAAMALEGPGKEH
                     NCRVHKAKEAGKGPIGHDDMVERFGLCETEEEESEDQIQMVPSDAVPEGKNKGKTKKG
                     RGRKNNYNAFSRRGLSDEEYEEYKKIREEKNGNYSIQEYLEDRQRYEEELAEVQAGGD
                     GGIGETEMEIRHRVFYKSKSKKHQQEQRRQLGLVTGSDIRKRKPIDWTPPKNEWADDD
                     REVDYNEKINFEAPPTLWSRVTKFGSGWGFWVSPTVFITTTHVVPTGVKEFFGEPLSS
                     IAIHQAGEFTQFRFSKKMRPDLTGMVLEEGCPEGTVCSVLIKRDSGELLPLAVRMGAI
                     ASMRIQGRLVHGQSGMLLTGANAKGMDLGTIPGDCGAPYVHKRGNDWVVCGVHAAATK
                     SGNTVVCAVQAGEGETALEGGDKGHYAGHEIVRYGSGPALSTKTKFWRSSPEPLPPGV
                     YEPAYLGGKDPRVQNGPSLQQVLRDQLKPFADPRGRMPEPGLLEAAVETVTSMLEQTM
                     DTPSPWSYADACQSLDKTTSSGYPHHKRKNDDWNGTTFVGELGEQAAHANNMYENAKH
                     MKPIYTAALKDELVKPEKIYQKVKKRLLWGADLGTVVRAARAFGPFCDAIKSHVIKLP
                     IKVGMNTIEDGPLIYAEHAKYKNHFDADYTAWDSTQNRQIMTESFSIMSRLTASPELA
                     EVVAQDLLAPSEMDVGDYVIRVKEGLPSGFPCTSQVNSINHWIITLCALSEATGLSPD
                     VVQSMSYFSFYGDDEIVSTDIDFDPARLTQILKEYGLKPTRPDKTEGPIQVRKNVDGL
                     VFLRRTISRDAAGFQGRLDRASIERQIFWTRGPNHSDPSETLVPHTQRKIQLISLLGE
                     ASLHGEKFYRKISSKVIHEIKTGGLEMYVPGWQAMFRWMRFHDLGLWTGDRDLLPEFV
                     NDDGV"
     CDS             5358..6950
                     /note="orf2"
                     /codon_start=1
                     /product="58 kd capsid protein"
                     /protein_id="AAB50466.2"
                     /translation="MMMASKDATSSVDGASGAGQLVPEVNASDPLAMDPVAGSSTAVA
                     TAGQVNPIDPWIINNFVQAPQGEFTISPNNTPGDVLFDLSLGPHLNPFLLHLSQMYNG
                     WVGNMRVRIMLAGNAFTAGKIIVSCIPPGFGSHNLTIAQATLFPHVIADVRTLDPIEV
                     PLEDVRNVLFHNNDRNQQTMRLVCMLYTPLRTGGGTGDSFVVAGRVMTCPSPDFNFLF
                     LVPPTVEQKTRPFTLPNLPLSSLSNSRAPLPISSMGISPDNVQSVQFQNGRCTLDGRL
                     VGTTPVSLSHVAKIRGTSNGTVINLTELDGTPFHPFEGPAPIGFPDLGGCDWHINMTQ
                     FGHSSQTQYDVDTTPDTFVPHLGSIQANGIGSGNYVGVLSWISPPSHPSGSQVDLWKI
                     PNYGSSITEATHLAPSVYPPGFGEVLVFFMSKMPGPGAYNLPCLLPQEYISHLASEQA
                     PTVGEAALLHYVDPDTGRNLGEFKAYPDGFLTCVPNGASSGPQQLPINGVFVFVSWVS
                     RFYQLKPVGTASSARGRLGLRR"
     CDS             6950..7588
                     /note="encodes small basic protein of unknown function"
                     /codon_start=1
                     /product="orf3"
                     /protein_id="AAB50467.1"
                     /translation="MAQAIIGAIAASTAGSALGAGIQVGGEAALQSQRYQQNLQLQEN
                     SFKHDREMIGYQVEASNQLLAKNLATRYSLLRAGGLTSADAARSVAGAPVTRIVDWNG
                     VRVSAPESSATTLRSGGFMSVPIPFASKQKQVQSSGISNPNYSPSSISRTTSWVESQN
                     SSRFGNLSPYHAEALNTVWLTPPGSTASSTLSSVPRGYFNTDRLPLFANNRR"
ORIGIN      
        1 gtgaatgatg atggcgtcaa aagacgtcgt tcctactgct gctagcagtg aaaatgctaa
       61 caacaatagt agtattaagt ctcgtctatt ggcgagactc aagggttcag gtggggctac
      121 gtccccaccc aactcgataa agataaccaa ccaagatatg gctctggggc tgattggaca
      181 ggtcccagcg ccaaaggcca catccgtcga tgtccctaaa caacagaggg atagaccacc
      241 acggactgtt gccgaagttc aacaaaattt gcgttggact gagagaccac aagaccagaa
      301 tgttaagacg tgggatgagc ttgaccacac aacaaaacaa cagatacttg atgaacacgc
      361 tgagtggttt gatgccggtg gcttaggtcc aagtacacta cccactagtc atgaacggta
      421 cacacatgag aatgatgaag gccaccaggt aaagtggtcg gctagggaag gtgtagacct
      481 tggcatatcc gggctcacga cggtgtctgg gcctgagtgg aatatgtgcc cgctaccacc
      541 agttgaccaa aggagcacga cacctgcaac tgagcccaca attggtgaca tgatcgaatt
      601 ctatgaaggg cacatctatc attatgctat atacataggt caaggcaaga cggtgggtgt
      661 acactcccct caagcagcct tctcaataac gaggatcacc atacagccca tatcagcttg
      721 gtggcgagtc tgttatgtcc cacaaccaaa acagaggctc acatacgacc aactcaaaga
      781 attagaaaat gaaccatggc cgtatgccgc agtcacgaac aactgcttcg aattttgttg
      841 ccaggtcatg tgcttggaag atacttggtt gcaaaggaag ctcatctcct ctggccggtt
      901 ttaccacccg acccaagatt ggtcccgaga cactccagaa ttccaacaag acagcaagtt
      961 agagatggtt agggatgcag tgctagccgc tataaatggg ttggtgtcgc ggccatttaa
     1021 agatcttctg ggtaagctca aacccttgaa cgtgcttaac ttactttcaa actgtgattg
     1081 gacgttcatg ggggtcgtgg agatggtggt cctcctttta gaactctttg gaatcttttg
     1141 gaacccacct gatgtttcca actttatagc ttcactcctg ccagatttcc atctacaggg
     1201 ccccgaggac cttgccaggg atctcgtgcc aatagtattg ggggggatcg gcttagccat
     1261 aggattcacc agagacaagg taagtaagat gatgaagaat gctgttgatg gacttcgtgc
     1321 ggcaacccag ctcggtcaat atggcctaga aatattctca ttactaaaga agtacttctt
     1381 cggtggtgat caaacagaga aaaccctaaa agatattgag tcagcagtta tagatatgga
     1441 agtactatca tctacatcag tgactcagct cgtgagggac aaacagtctg cacgggctta
     1501 tatggccatc ttagataatg aagaagaaaa ggcaaggaaa ttatctgtca ggaatgccga
     1561 cccacacgta gtatcctcta ccaatgctct catatcccgg atctcaatgg ctagggctgc
     1621 attggccaag gctcaagctg aaatgaccag caggatgcgt cctgtggtca ttatgatgtg
     1681 tgggccccct ggtataggta aaaccaaggc agcagaacat ctggctaaac gcctagccaa
     1741 tgagatacgg cctggtggta aggttgggct ggtcccacgg gaggcagtgg atcattggga
     1801 tggatatcac ggagaggaag tgatgctgtg ggacgactat ggaatgacaa agatacagga
     1861 agactgtaat aaactgcaag ccatagccga ctcagccccc ctaacactca attgtgaccg
     1921 aatagaaaac aagggaatgc aatttgtgtc tgatgctata gtcatcacca ccaatgctcc
     1981 tggcccagcc ccagtggact ttgtcaacct cgggcctgtt tgccgaaggg tggacttcct
     2041 tgtgtattgc acggcacctg aagttgaaca cacgaggaaa gtcagtcctg gggacacaac
     2101 tgcactgaaa gactgcttca agcccgattt ctcacatcta aaaatggagt tggctcccca
     2161 agggggcttt gataaccaag ggaatacccc gtttggtaag ggtgtgatga agcccaccac
     2221 cataaacagg ctgttaatcc aggctgtagc cttgacgatg gagagacagg atgagttcca
     2281 actccagggg cctacgtatg actttgatac tgacagagta gctgcgttca cgaggatggc
     2341 ccgagccaac gggttgggtc tcatatccat ggcctcccta ggcaaaaagc tacgcagtgt
     2401 caccactatt gaaggattaa agaatgctct atcaggctat aaaatatcaa aatgcagtat
     2461 acaatggcag tcaagggtgt acattataga atcagatggt gccagtgtac aaatcaaaga
     2521 agacaagcaa gctttgaccc ctctgcagca gacaattaac acggcctcac ttgccatcac
     2581 tcgactcaaa gcagctaggg ctgtggcata cgcttcatgt ttccagtccg ccataactac
     2641 catactacaa atggcgggat ctgcgctcgt tattaatcga gcggtcaagc gtatgtttgg
     2701 tacccgtaca gcagccatgg cattagaagg acctgggaaa gaacataatt gcagggtcca
     2761 taaggctaag gaagctggaa aggggcccat aggtcatgat gacatggtag aaaggtttgg
     2821 cctatgtgaa actgaagagg aggagagtga ggaccaaatt caaatggtac caagtgatgc
     2881 cgtcccagaa ggaaagaaca aaggcaagac caaaaaggga cgtggtcgca aaaataacta
     2941 taatgcattc tctcgccgtg gtctgagtga tgaagaatat gaagagtaca aaaagatcag
     3001 agaagaaaag aatggcaatt atagtataca agaatacttg gaggaccgcc aacgatatga
     3061 ggaagaatta gcagaggtac aggcaggtgg tgatggtggc ataggagaaa ctgaaatgga
     3121 aatccgtcac agggtcttct ataaatccaa gagtaagaaa caccaacaag agcaacggcg
     3181 acaacttggt ctagtgactg gatcagacat cagaaaacgt aagcccattg actggacccc
     3241 gccaaagaat gaatgggcag atgatgacag agaggtggat tataatgaaa agatcaattt
     3301 tgaagctccc ccgacactat ggagccgagt cacaaagttt ggatcaggat ggggcttttg
     3361 ggtcagcccg acagtgttca tcacaaccac acatgtagtg ccaactggtg tgaaagaatt
     3421 ctttggtgag cccctatcta gtatagcaat ccaccaagca ggtgagttca cacaattcag
     3481 gttctcaaag aaaatgcgcc ctgacttgac aggtatggtc cttgaagaag gttgccctga
     3541 agggacagtc tgctcagtcc taattaaacg ggattcgggt gaactacttc cgctagccgt
     3601 ccgtatgggg gctattgcct ccatgaggat acagggtcgg cttgtccatg gccaatcagg
     3661 gatgttactg acaggggcca atgcaaaggg gatggatctt ggcactatac caggagactg
     3721 cggggcacca tacgtccaca agcgcgggaa tgactgggtt gtgtgtggag tccacgctgc
     3781 agccacaaag tcaggcaaca ccgtggtctg cgctgtacag gctggagagg gcgaaaccgc
     3841 actagaaggt ggagacaagg ggcattatgc cggccacgag attgtgaggt atggaagtgg
     3901 cccagcactg tcaactaaaa caaaattctg gaggtcctcc ccagaaccac tgccccccgg
     3961 agtatatgag ccagcatacc tggggggcaa ggacccccgt gtacagaatg gcccatccct
     4021 acaacaggta ctacgtgacc aactgaaacc ctttgcggac ccccgcggcc gcatgcctga
     4081 gcctggccta ctggaggctg cggttgagac tgtaacatcc atgttagaac agacaatgga
     4141 taccccaagc ccgtggtctt acgctgatgc ctgccaatct cttgacaaaa ctactagttc
     4201 ggggtaccct caccataaaa ggaagaatga tgattggaat ggcaccacct tcgttggaga
     4261 gctcggtgag caagctgcac acgccaacaa tatgtatgag aatgctaaac atatgaaacc
     4321 catttacact gcagccttaa aagatgaact agtcaagcca gaaaagattt atcaaaaagt
     4381 caagaagcgt ctactatggg gcgccgatct cggaacagtg gtcagggccg cccgggcttt
     4441 tggcccattt tgtgacgcta taaaatcaca tgtcatcaaa ttgccaataa aagttggcat
     4501 gaacacaata gaagatggcc ccctcatcta tgctgagcat gctaaatata agaatcattt
     4561 tgatgcagat tatacagcat gggactcaac acaaaataga caaattatga cagaatcctt
     4621 ctccattatg tcgcgcctta cggcctcacc agaattggcc gaggttgtgg cccaagattt
     4681 gctagcacca tctgagatgg atgtaggtga ttatgtcatc agggtcaaag aggggctgcc
     4741 atctggattc ccatgtactt cccaggtgaa cagcataaat cactggataa ttactctctg
     4801 tgcactgtct gaggccactg gtttatcacc tgatgtggtg caatccatgt catatttctc
     4861 attttatggt gatgatgaga ttgtgtcaac tgacatagat tttgacccag cccgcctcac
     4921 tcaaattctc aaggaatatg gcctcaaacc aacaaggcct gacaaaacag aaggaccaat
     4981 acaagtgagg aaaaatgtgg atggactggt cttcttgcgg cgcaccattt cccgtgatgc
     5041 ggcagggttc caaggcaggt tagatagggc ttcgattgaa cgccaaatct tctggacccg
     5101 cgggcccaat cattcagatc catcagagac tctagtgcca cacactcaaa gaaaaataca
     5161 gttgatttca cttctagggg aagcttcact ccatggtgag aaattttaca gaaagatttc
     5221 cagcaaggtc atacatgaaa tcaagactgg tggattggaa atgtatgtcc caggatggca
     5281 ggccatgttc cgctggatgc gcttccatga cctcggattg tggacaggag atcgcgatct
     5341 tctgcccgaa ttcgtaaatg atgatggcgt ctaaggacgc tacatcaagc gtggatggcg
     5401 ctagtggcgc tggtcagttg gtaccggagg ttaatgcttc tgaccctctt gcaatggatc
     5461 ctgtagcagg ttcttcgaca gcagtcgcga ctgctggaca agttaatcct attgatccct
     5521 ggataattaa taattttgtg caagcccccc aaggtgaatt tactatttcc ccaaataata
     5581 cccccggtga tgttttgttt gatttgagtt tgggtcccca tcttaatcct ttcttgctcc
     5641 atctatcaca aatgtataat ggttgggttg gtaacatgag agtcaggatt atgctagctg
     5701 gtaatgcctt tactgcgggg aagataatag tttcctgcat accccctggt tttggttcac
     5761 ataatcttac tatagcacaa gcaactctct ttccacatgt gattgctgat gttaggactc
     5821 tagaccccat tgaggtgcct ttggaagatg ttaggaatgt tctctttcat aataatgata
     5881 gaaatcaaca aaccatgcgc cttgtgtgca tgctgtacac ccccctccgc actggtggtg
     5941 gtactggtga ttcttttgta gttgcagggc gagttatgac ttgccccagt cctgatttta
     6001 atttcttgtt tttagtccct cctacggtgg agcagaaaac caggcccttc acactcccaa
     6061 atctgccatt gagttctctg tctaactcac gtgcccctct cccaatcagt agtatgggca
     6121 tttccccaga caatgtccag agtgtgcagt tccaaaatgg tcggtgtact ctggatggcc
     6181 gcctggttgg caccacccca gtttcattgt cacatgttgc caagataaga gggacctcca
     6241 atggcactgt aatcaacctt actgaattgg atggcacacc ctttcaccct tttgagggcc
     6301 ctgcccccat tgggtttcca gacctcggtg gttgtgattg gcatatcaat atgacacagt
     6361 ttggccattc tagccagacc cagtatgatg tagacaccac ccctgacact tttgtccccc
     6421 atcttggttc aattcaggca aatggcattg gcagtggtaa ttatgttggt gttcttagct
     6481 ggatttcccc cccatcacac ccgtctggct cccaagttga cctttggaag atccccaatt
     6541 atgggtcaag tattacggag gcaacacatc tagccccttc tgtatacccc cctggtttcg
     6601 gagaggtatt ggtctttttc atgtcaaaaa tgccaggtcc tggtgcttat aatttgccct
     6661 gtctattacc acaagagtac atttcacatc ttgctagtga acaagcccct actgtaggtg
     6721 aggctgccct gctccactat gttgaccctg ataccggtcg gaatcttggg gaattcaaag
     6781 cataccctga tggtttcctc acttgtgtcc ccaatggggc tagctcgggt ccacaacagc
     6841 tgccgatcaa tggggtcttt gtctttgttt catgggtgtc cagattttat caattaaagc
     6901 ctgtgggaac tgccagctcg gcaagaggta ggcttggtct gcgccgataa tggcccaagc
     6961 cataattggt gcaattgctg cttccacagc aggtagtgct ctgggagcgg gcatacaggt
     7021 tggtggcgaa gcggccctcc aaagccaaag gtatcaacaa aatttgcaac tgcaagaaaa
     7081 ttcttttaaa catgacaggg aaatgattgg gtatcaggtt gaagcttcaa atcaattatt
     7141 ggctaaaaat ttggcaacta gatattcact cctccgtgct gggggtttga ccagtgctga
     7201 tgcagcaaga tctgtggcag gagctccagt cacccgcatt gtagattgga atggcgtgag
     7261 agtgtctgct cccgagtcct ctgctaccac attgagatcc ggtggcttca tgtcagttcc
     7321 cataccattt gcctctaagc aaaaacaggt tcaatcatct ggtattagta atccaaatta
     7381 ttccccttca tccatttctc gaaccactag ttgggtcgag tcacaaaact catcgagatt
     7441 tggaaatctt tctccatacc acgcggaggc tctcaataca gtgtggttga ctccacccgg
     7501 ttcaacagcc tcttctacac tgtcttctgt gccacgtggt tatttcaata cagacaggtt
     7561 gccattattc gcaaataata ggcgatgatg ttgtaatatg aaatgtgggc atcatattca
     7621 tttaattagg tttaattagg tttaatttga tgtt
//


LOCUS       KF039733                7600 bp ss-RNA     linear   VRL 24-OCT-2013
DEFINITION  Norovirus Hu/GI.1/CHA9A004_20110426/2011/USA, complete genome.
ACCESSION   KF039733
VERSION     KF039733.1
DBLINK      BioProject: PRJNA70471
KEYWORDS    .
SOURCE      Norovirus Hu/GI.1/CHA9A004_20110426/2011/USA
  ORGANISM  Norovirus Hu/GI.1/CHA9A004_20110426/2011/USA
            Viruses; Riboviria; Orthornavirae; Pisuviricota; Pisoniviricetes;
            Picornavirales; Caliciviridae; Norovirus.
REFERENCE   1  (bases 1 to 7600)
  AUTHORS   Madupu,R., Halpin,R.A., Ransier,A., Fedorova,N., Tsitrin,T.,
            McLellan,M., Stockwell,T., Amedeo,P., Appalla,L., Bishop,B.,
            Edworthy,P., Gupta,N., Hoover,J., Katzel,D., Li,K., Schobel,S.,
            Shrivastava,S., Thovarai,V., Wang,S., Kim,M., Bok,K.,
            Sosnovtsev,S.V., Wentworth,D.E. and Green,K.Y.
  TITLE     Direct Submission
  JOURNAL   Submitted (10-MAY-2013) J. Craig Venter Institute, 9704 Medical
            Center Drive, Rockville, MD 20850, USA
COMMENT     This work was supported by the National Institute of Allergy and
            Infectious Diseases (NIAID), Genome Sequencing Centers for
            Infectious Diseases (GSCID) program.
            The genome sequence was generated using overlapping PCR amplicons
            spanning the genome. The amplicons were pooled by sample and then
            barcoded and sequenced using Next Generation Sequencing platforms.
            The consensus sequences of the internal PCR primer hybridization
            sites were manually verified using reads from amplicons that
            spanned across the sites.
            Genome sequence lacks part of non-coding region.
            
            ##Genome-Assembly-Data-START##
            Current Finishing Status :: Finished
            Assembly Method          :: clc_ref_assemble_long v. 3.22.55705
            Genome Coverage          :: 766.8x
            Sequencing Technology    :: Illumina; 454
            ##Genome-Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..7600
                     /organism="Norovirus Hu/GI.1/CHA9A004_20110426/2011/USA"
                     /mol_type="genomic RNA"
                     /strain="Hu/GI.1/CHA9A004_20110426/2011/USA"
                     /host="Homo sapiens"
                     /db_xref="taxon:1335904"
                     /lab_host="Pan troglodytes"
                     /country="USA: Maryland"
                     /collection_date="26-Apr-2011"
                     /PCR_primers="fwd_name: Ampl_1_Forward, fwd_seq:
                     tgtaaaacgacggccagtgaatgatgatggcgtcaa, rev_name:
                     Ampl_1_Reverse, rev_seq:
                     caggaaacagctatgaccgagcctctgttttggttgtg"
                     /PCR_primers="fwd_name: Ampl_2_Forward, fwd_seq:
                     tgtaaaacgacggccagtcacaattggtgacatgatcg, rev_name:
                     Ampl_2_Reverse, rev_seq:
                     caggaaacagctatgacctttctctgtttgatcaccacc"
                     /PCR_primers="fwd_name: Ampl_3_Forward, fwd_seq:
                     tgtaaaacgacggccagttcggtcaatatggcctagaa, rev_name:
                     Ampl_3_Reverse, rev_seq:
                     caggaaacagctatgaccttatcaaagcccccttgg"
                     /PCR_primers="fwd_name: Ampl_4_Forward, fwd_seq:
                     tgtaaaacgacggccagtcggtggtgatcaaacagag, rev_name:
                     Ampl_4_Reverse, rev_seq:
                     caggaaacagctatgaccccttaccaaacggggtattc"
                     /PCR_primers="fwd_name: Ampl_5_Forward, fwd_seq:
                     tgtaaaacgacggccagtgaataccccgtttggtaagg, rev_name:
                     Ampl_5_Reverse, rev_seq:
                     caggaaacagctatgacctcttcatcactcagaccacg"
                     /PCR_primers="fwd_name: Ampl_6_Forward, fwd_seq:
                     tgtaaaacgacggccagtgacgtggtcgcaaaaataac, rev_name:
                     Ampl_6_Reverse, rev_seq:
                     caggaaacagctatgaccacacacaacccagtcattcc"
                     /PCR_primers="fwd_name: Ampl_7_Forward, fwd_seq:
                     tgtaaaacgacggccagttcagagaagaaaagaatggca, rev_name:
                     Ampl_7_Reverse, rev_seq:
                     caggaaacagctatgaccgtgttgcctgactttgtgg"
                     /PCR_primers="fwd_name: Ampl_8_Forward, fwd_seq:
                     tgtaaaacgacggccagtttgacaggtatggtccttgaaga, rev_name:
                     Ampl_8_Reverse, rev_seq:
                     caggaaacagctatgaccccaccttctagtgcggtttcg"
                     /PCR_primers="fwd_name: Ampl_9_Forward, fwd_seq:
                     tgtaaaacgacggccagtactataccaggagactgc, rev_name:
                     Ampl_9_Reverse, rev_seq:
                     caggaaacagctatgacctagtacctgttgtagggatg"
                     /PCR_primers="fwd_name: Ampl_10_Forward, fwd_seq:
                     tgtaaaacgacggccagtcaccgtggtctgcgctgta, rev_name:
                     Ampl_10_Reverse, rev_seq:
                     caggaaacagctatgaccttggggtatccattgtctgttc"
                     /PCR_primers="fwd_name: Ampl_11_Forward, fwd_seq:
                     tgtaaaacgacggccagtacccccgtgtacagaatggc, rev_name:
                     Ampl_11_Reverse, rev_seq:
                     caggaaacagctatgaccggctgcagtgtaaatgggtttc"
                     /PCR_primers="fwd_name: Ampl_12_Forward, fwd_seq:
                     tgtaaaacgacggccagtaccataaaaggaagaatg, rev_name:
                     Ampl_12_Reverse, rev_seq:
                     caggaaacagctatgaccaatgattcttatatttagca"
                     /PCR_primers="fwd_name: Ampl_13_Forward, fwd_seq:
                     tgtaaaacgacggccagtgcacacgccaacaatatgta, rev_name:
                     Ampl_13_Reverse, rev_seq:
                     caggaaacagctatgacccagaagatttggcgttcaat"
                     /PCR_primers="fwd_name: Ampl_14_Forward, fwd_seq:
                     tgtaaaacgacggccagtcactgcagccttaaaagatga, rev_name:
                     Ampl_14_Reverse, rev_seq:
                     caggaaacagctatgaccttttctttgagtgtgtggca"
                     /PCR_primers="fwd_name: Ampl_15_Forward, fwd_seq:
                     tgtaaaacgacggccagtccaatcattcagatccatca, rev_name:
                     Ampl_15_Reverse, rev_seq:
                     caggaaacagctatgaccatcaccagtaccaccaccag"
                     /PCR_primers="fwd_name: Ampl_16_Forward, fwd_seq:
                     tgtaaaacgacggccagttgcctttggaagatgttagg, rev_name:
                     Ampl_16_Reverse, rev_seq:
                     caggaaacagctatgacccctggcatttttgacatga"
                     /PCR_primers="fwd_name: Ampl_17_Forward, fwd_seq:
                     tgtaaaacgacggccagtatccccaattatgggtcaag, rev_name:
                     Ampl_17_Reverse, rev_seq:
                     caggaaacagctatgaccaggcaaatggtatgggaact"
                     /PCR_primers="fwd_name: Ampl_18_Forward, fwd_seq:
                     tgtaaaacgacggccagttagccccttctgtatacccc, rev_name:
                     Ampl_18_Reverse, rev_seq:
                     caggaaacagctatgacctgaatakrrtgcycac"
                     /note="Chimpanzee passage of Norovirus
                     Hu/GI.1/8FIIa/1968/USA;
                     genotype: GI.1"
     gene            1..5370
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
     CDS             1..5370
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
                     /note="genome polyprotein"
                     /codon_start=1
                     /product="nonstructural polyprotein"
                     /protein_id="AGM33231.1"
                     /translation="MMMASKDVVPTAASSENANNNSSIKSRLLARLKGSGGATSPPNS
                     IKITNQDMALGLIGQVPAPKATSVDVPKQQRDRPPRTVAEVQQNLRWTERPQDQNVKT
                     WDELDHTTKQQILDGHAEWFDAGGLGPSTLPTSHERYTHENDEGHQVKWSAREGVDLG
                     ISGLTTVSGPEWNMCPLPPVDQRSTTPATEPTIGDMIEFYEGHIYHYAIYIGQGKTVG
                     VHSPQAAFSITRITIQPISAWWRVCYVPQPKQRLTYDQLKELENEPWPYAAVTNNCFE
                     FCCQVMCLEDTWLQRKLISSGRFYHPTQDWSRDTPEFQQDSKLEMVRDAVLAAINGLV
                     SRPFKDLLGKLKPLNVLNLLSNCDWTFMGVVEMVVLLLELFGIFWNPPDVSNFIASLL
                     PDFHLQGPEDLARDLVPIVLGGIGLAIGFTRDKVSKMMKNAVDGLRAATQLGQYGLEI
                     FSLLKKYFFGGDQTEKTLKDIESAVIDMEVLSSTSVTQLVRDKQSARAYMAILDNEEE
                     KARKLSVRNADPHVVSSTNALISRISMARAALAKAQAEMTSRMRPVVIMMCGPPGIGK
                     TKAAEHLAKRLANEIRPGGKVGLVPREAVDHWDGYHGEEVMLWDDYGMTKIQEDCNKL
                     QAIADSAPLTLNCDRIENKGMQFVSDAIVITTNAPGPAPVDFVNLGPVCRRVDFLVYC
                     TAPEVEHTRKVSPGDTTALKDCFKPDFSHLKMELAPQGGFDNQGNTPFGKGVMKPTTI
                     NRLLIQAVALTMERQDEFQLQGPTYDFDTDRVAAFTRMARANGLGLISMASLGKKLRS
                     VTTIEGLKNALSGYKISKCSIQWQSRVYIIESDGASVQIKEDKQALTPLQQTINTASL
                     AITRLKAARAVAYASCFQSAITTILQMAGSALVINRAVKRMFGTRTAAMALEGPGKEH
                     NCRVHKAKEAGKGPIGHDDMVERFGLCETEEEESEDQIQMVPSDAVPEGKNKGKTKKG
                     RGRKNNYNAFSRRGLSDEEYEEYKKIREEKNGNYSIQEYLEDRQRYEEELAEVQAGGD
                     GGIGETEMEIRHRVFYKSKSKKHQQEQRRQLGLVTGSDIRKRKPIDWTPPKSEWADDD
                     REVDYNEKINFEAPPTLWSRVTKFGSGWGFWVSPTVFITTTHVVPTGVKEFFGEPLSS
                     IAIHQAGEFTQFRFSKKMRPDLTGMVLEEGCPEGTVCSVLIKRDSGELLPLAVRMGAI
                     ASMRIQGRLVHGQSGMLLTGANAKGMDLGTIPGDCGAPYVHKRGNDWVVCGVHAAATK
                     SGNTVVCAVQAGEGETALEGGDKGHYAGHEIVRYGSGPALSTKTKFWRSSPEPLPPGV
                     YEPAYLGGKDPRVQNGPSLQQVLRDQLKPFADPRGRMPEPGLLEAAVETVTSMLEQTM
                     DTPSPWSYADACQSLDKTTSSGYPHHKRKNDDWNGTTFVGELGEQAAHANNMYENAKH
                     MKPIYTAALKDELVKPEKIYQKVKKRLLWGADLGTVVRAARAFGPFCDAIKSHVIKLP
                     IKVGMNTIEDGPLIYAEHAKYKNHFDADYTAWDSTQNRQIMTESFSIMSRLTASPELA
                     EVVAQDLLAPSEMDVGDYVIRVKEGLPSGFPCTSQVNSINHWITTLCALSEATGLSPD
                     VVQSMSYFSFYGDDEIVSTDIDFDPARLTQILKEYGLKPTRPDKTEGPIQVRKNVDGL
                     VFLRRTISRDAAGFQGRLDRASIERQIFWTRGPNHSDPSETLVPHTQRKIQLISLLGE
                     ASLHGEKFYRKISSKVIHEIKTGGLEMYVPGWQAMFRWMRFHDLGLWTGDRDLLPEFV
                     NDDGV"
     mat_peptide     1..1194
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
                     /product="protein p37"
     mat_peptide     1195..2283
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
                     /product="NTPase"
                     /note="p40"
     mat_peptide     2284..2886
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
                     /product="protein p20"
     mat_peptide     2887..3300
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
                     /product="viral genome-linked protein"
                     /note="VPg"
     mat_peptide     3301..3843
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
                     /product="3C-like protease"
                     /note="3CLpro; calicivirin"
     mat_peptide     3844..5367
                     /gene="POL"
                     /locus_tag="H649_41405gpPOL"
                     /product="RNA-directed RNA polymerase"
     primer_bind     1..16
     gene            5354..6946
                     /gene="VP1"
                     /locus_tag="H649_41405gpVP1"
     CDS             5354..6946
                     /gene="VP1"
                     /locus_tag="H649_41405gpVP1"
                     /note="p59"
                     /codon_start=1
                     /product="capsid protein VP1"
                     /protein_id="AGM33232.1"
                     /translation="MMMASKDATSSVDGASGAGQLVPEVNASDPLAMDPVAGSSTAVA
                     TAGQVNPIDPWIINNFVQAPQGEFTISPNNTPGDVLFDLSLGPHLNPFLLHLSQMYNG
                     WVGNMRVRIMLAGNAFTAGKIIVSCIPPGFGSHNLTIAQATLFPHVIADVRTLDPIEV
                     PLEDVRNVLFHNNDRNQQTMRLVCMLYTPLRTGGGTGDSFVVAGRVMTCPSPDFNFLF
                     LVPPTVEQKTRPFTLPNLPLSSLSNSRAPLPISSMGISPDNVQSVQFQNGRCTLDGRL
                     VGTTPVSLSHVAKIRGTSNGTVINLTELDGTPFHPFEGPAPIGFPDLGGCDWHINMTQ
                     FGHSSQTQYDVDTTPDTFVPHLGSIQANGIGSGNYVGVLSWISPPSRPSGSQVDLWKI
                     PNYGSSITEATHLAPSVYPPGFGEVLVFFMSKMPGPGAYNLPCLLPQEYISHLASEQA
                     PTVGEAALLHYVDPDTGRNLGEFKAYPDGFLTCVPNGASSGPQQLPINGVFVFVSWVS
                     RFYQLKPVGTASSARGRLGLRR"
     gene            6946..7584
                     /gene="VP2"
                     /locus_tag="H649_41405gpVP2"
     CDS             6946..7584
                     /gene="VP2"
                     /locus_tag="H649_41405gpVP2"
                     /note="minor capsid protein; basic protein"
                     /codon_start=1
                     /product="capsid protein VP2"
                     /protein_id="AGM33233.1"
                     /translation="MAQAIIGAIAASTAGSALGAGIQVGGEAALQSQRYQQNLQLQEN
                     SFKHDREMIGYQVEASNQLLAKNLATRYSLLRAGGLTSADAARSVAGVPVTRIVDWNG
                     VRVSAPESSATTLRSGGFMSVPIPFASKQKQIQSSGISNPNYSPSSISRTTSWVESQN
                     SSRFGNLSPYHAEALNTVWLTPPGSTASSTLSSVPRGYFNTDRLPLFANNRR"
ORIGIN      
        1 atgatgatgg cgtcaaaaga cgtcgttcct actgctgcta gcagtgaaaa tgctaacaac
       61 aatagtagta ttaagtctcg tctattggcg agactcaagg gttcaggtgg ggctacgtcc
      121 ccacccaact cgataaagat aaccaaccaa gatatggctc tggggctgat tggacaggtc
      181 ccagcgccaa aggccacatc cgtcgatgtc cctaaacaac agagggatag accaccacgg
      241 actgttgccg aagttcaaca aaatttgcgt tggactgaga gaccacaaga ccagaatgtt
      301 aagacgtggg atgagcttga ccacacaaca aaacaacaga tacttgatgg acacgctgag
      361 tggtttgatg ccggtggctt aggtccaagt acactaccca ctagtcatga acggtacaca
      421 catgagaatg atgaaggcca ccaggtaaag tggtcggcta gggaaggtgt agaccttggc
      481 atatccgggc tcacgacggt gtctgggcct gagtggaata tgtgcccgct accaccagtt
      541 gaccaaagga gcacgacacc tgcaactgag cccacaattg gtgacatgat cgaattctat
      601 gaagggcaca tctatcatta tgctatatac ataggtcaag gcaagacggt gggtgtacac
      661 tcccctcaag cagccttctc aataacgagg atcaccatac agcccatatc agcttggtgg
      721 cgagtctgtt atgtcccaca accaaaacag aggctcacat acgaccaact caaagaatta
      781 gaaaatgaac catggccgta tgccgcagtc acgaacaact gcttcgaatt ttgttgccag
      841 gtcatgtgct tggaagatac ttggttgcaa aggaagctca tctcctctgg ccggttttac
      901 cacccgaccc aagattggtc ccgagacact ccagaattcc aacaagacag caagttagag
      961 atggttaggg atgcagtgct agccgctata aatgggttgg tgtcgcggcc atttaaagat
     1021 cttctgggta agctcaaacc cttgaacgtg cttaacttac tttcaaactg tgattggacg
     1081 ttcatggggg tcgtggagat ggtggtcctc cttttagaac tctttggaat cttttggaac
     1141 ccacctgatg tttccaactt tatagcttca ctcctgccag atttccatct acagggcccc
     1201 gaggaccttg ccagggatct cgtgccaata gtattggggg ggatcggctt agccatagga
     1261 ttcaccagag acaaggtaag taagatgatg aagaatgctg ttgatggact tcgtgcggca
     1321 acccagctcg gtcaatatgg cctagaaata ttctcattac taaagaagta cttcttcggt
     1381 ggtgatcaaa cagagaaaac cctaaaagat attgagtcag cagttataga tatggaagta
     1441 ctatcatcta catcagtgac tcagctcgtg agggacaaac agtctgcacg ggcttatatg
     1501 gccatcttag ataatgaaga agaaaaggca aggaaattat ctgtcaggaa tgccgaccca
     1561 cacgtagtat cctctaccaa tgctctcata tcccggatct caatggctag ggctgcattg
     1621 gccaaggctc aagctgaaat gaccagcagg atgcgtcctg tggtcattat gatgtgtggg
     1681 ccccctggta taggtaaaac caaggcagca gaacatctgg ctaaacgcct agccaatgag
     1741 atacggcctg gtggtaaggt tgggctggtc ccacgggagg cagtggatca ttgggatgga
     1801 tatcacggag aggaagtgat gctgtgggac gactatggaa tgacaaagat acaggaagac
     1861 tgtaataaac tgcaagccat agccgactca gcccccctaa cactcaattg tgaccgaata
     1921 gaaaacaagg gaatgcaatt tgtgtctgat gctatagtca tcaccaccaa tgctcctggc
     1981 ccagccccag tggactttgt caacctcggg cctgtttgcc gaagggtgga cttccttgtg
     2041 tattgcacgg cacctgaagt tgaacacacg aggaaagtca gtcctgggga cacaactgca
     2101 ctgaaagact gcttcaagcc cgatttctca catctaaaaa tggagttggc tccccaaggg
     2161 ggctttgata accaagggaa taccccgttt ggtaagggtg tgatgaagcc caccaccata
     2221 aacaggctgt taatccaggc tgtagccttg acgatggaga gacaggatga gttccaactc
     2281 caggggccta cgtatgactt tgatactgac agagtagctg cgttcacgag gatggcccga
     2341 gccaacgggt tgggtctcat atccatggcc tccctaggca aaaagctacg cagtgtcacc
     2401 actattgaag gattaaagaa tgctctatca ggctataaaa tatcaaaatg cagtatacaa
     2461 tggcagtcaa gggtgtacat tatagaatca gatggtgcca gtgtacaaat caaagaagac
     2521 aagcaagctt tgacccctct gcagcagaca attaacacgg cctcacttgc catcactcga
     2581 ctcaaagcag ctagggctgt ggcatacgct tcatgtttcc agtccgccat aactaccata
     2641 ctacaaatgg cgggatctgc gctcgttatt aatcgagcgg tcaagcgtat gtttggtacc
     2701 cgtacagcag ccatggcatt agaaggacct gggaaagaac ataattgcag ggtccataag
     2761 gctaaggaag ctggaaaggg gcccataggt catgatgaca tggtagaaag gtttggccta
     2821 tgtgaaactg aagaggagga gagtgaggac caaattcaaa tggtaccaag tgatgccgtc
     2881 ccagaaggaa agaacaaagg caagaccaaa aagggacgtg gtcgcaaaaa taactataat
     2941 gcattctctc gccgtggtct gagtgatgaa gaatatgaag agtacaaaaa gatcagagaa
     3001 gaaaagaatg gcaattatag tatacaagaa tacttggagg accgccaacg atatgaggaa
     3061 gaattagcag aggtacaggc aggtggtgat ggtggcatag gagaaactga aatggaaatc
     3121 cgtcacaggg tcttctataa atccaagagt aagaaacacc aacaagagca acggcgacaa
     3181 cttggtctag tgactggatc agacatcaga aaacgtaagc ccattgactg gaccccgcca
     3241 aagagtgaat gggcagatga tgacagagag gtggattata atgaaaagat caattttgaa
     3301 gctcccccga cactatggag ccgagtcaca aagtttggat caggatgggg cttttgggtc
     3361 agcccgacag tgttcatcac aaccacacat gtagtgccaa ctggtgtgaa agaattcttt
     3421 ggtgagcccc tatctagtat agcaatccac caagcaggtg agttcacaca attcaggttc
     3481 tcaaagaaaa tgcgccctga cttgacaggt atggtccttg aagaaggttg ccctgaaggg
     3541 acagtctgct cagtcctaat taaacgggat tcgggtgaac tacttccgct agccgtccgt
     3601 atgggggcta ttgcctccat gaggatacag ggtcggcttg tccatggcca atcagggatg
     3661 ttactgacag gggccaatgc aaaggggatg gatcttggca ctataccagg agactgcggg
     3721 gcaccatacg tccacaagcg cgggaatgac tgggttgtgt gtggagtcca cgctgcagcc
     3781 acaaagtcag gcaacaccgt ggtctgcgct gtacaggctg gagagggcga aaccgcacta
     3841 gaaggtggag acaaggggca ttatgccggc cacgagattg tgaggtatgg aagtggccca
     3901 gcactgtcaa ctaaaacaaa attctggagg tcctccccag aaccactgcc ccccggagta
     3961 tatgagccag catacctggg gggcaaggac ccccgtgtac agaatggccc atccctacaa
     4021 caggtactac gtgaccaact gaaacccttt gcggaccccc gcggccgcat gcctgagcct
     4081 ggcctactgg aggctgcggt tgagactgta acatccatgt tagaacagac aatggatacc
     4141 ccaagcccgt ggtcttacgc tgatgcctgc caatctcttg acaaaactac tagttcgggg
     4201 taccctcacc ataaaaggaa gaatgatgat tggaatggca ccaccttcgt tggagagctc
     4261 ggtgagcaag ctgcacacgc caacaatatg tatgagaatg ctaaacatat gaaacccatt
     4321 tacactgcag ccttaaaaga tgaactagtc aagccagaaa agatttatca aaaagtcaag
     4381 aagcgtctac tatggggcgc cgatctcgga acagtggtca gggccgcccg ggcttttggc
     4441 ccattttgtg acgctataaa atcacatgtc atcaaattgc caataaaagt tggcatgaac
     4501 acaatagaag atggccccct catctatgct gagcatgcta aatataagaa tcattttgat
     4561 gcagattata cagcatggga ctcaacacaa aatagacaaa ttatgacaga atccttctcc
     4621 attatgtcgc gccttacggc ctcaccagaa ttggccgagg ttgtggccca agatttgcta
     4681 gcaccatctg agatggatgt aggtgattat gtcatcaggg tcaaagaggg gctgccatct
     4741 ggattcccat gtacttccca ggtgaacagc ataaatcact ggataactac tctctgtgca
     4801 ctgtctgagg ccactggttt atcacctgat gtggtgcaat ccatgtcata tttctcattt
     4861 tatggtgatg atgagattgt gtcaactgac atagattttg acccagcccg cctcactcaa
     4921 attctcaagg aatatggcct caaaccaaca aggcctgaca aaacagaagg accaatacaa
     4981 gtgaggaaaa atgtggatgg actggtcttc ttgcggcgca ccatttcccg tgatgcggca
     5041 gggttccaag gcaggttaga tagggcttcg attgaacgcc aaatcttctg gacccgcggg
     5101 cccaatcatt cagatccatc agagactcta gtgccacaca ctcaaagaaa aatacagttg
     5161 atttcacttc taggggaagc ttcactccat ggtgagaaat tttacagaaa gatttccagc
     5221 aaggtcatac atgaaatcaa gactggtgga ttggaaatgt atgtcccagg atggcaggcc
     5281 atgttccgct ggatgcgctt ccatgacctc ggattgtgga caggagatcg cgatcttctg
     5341 cccgaattcg taaatgatga tggcgtctaa ggacgctaca tcaagcgtgg atggcgctag
     5401 tggcgctggt cagttggtac cggaggttaa tgcttctgac cctcttgcaa tggatcctgt
     5461 agcaggttct tcgacagcag tcgcgactgc tggacaagtt aatcctattg atccctggat
     5521 aattaataat tttgtgcaag ccccccaagg tgaatttact atttccccaa ataatacccc
     5581 cggtgatgtt ttgtttgatt tgagtttggg tccccatctt aatcctttct tgctccatct
     5641 atcacaaatg tataatggtt gggttggtaa catgagagtc aggattatgc tagctggtaa
     5701 tgcctttact gcggggaaga taatagtttc ctgcataccc cctggttttg gttcacataa
     5761 tcttactata gcacaagcaa ctctctttcc acatgtgatt gctgatgtta ggactctaga
     5821 ccccattgag gtgcctttgg aagatgttag gaatgttctc tttcataata atgatagaaa
     5881 tcaacaaacc atgcgccttg tgtgcatgct gtacaccccc ctccgcactg gtggtggtac
     5941 tggtgattct tttgtagttg cagggcgagt tatgacttgc cccagtcctg attttaattt
     6001 cttgtttcta gtccctccta cggtggagca gaaaaccagg cccttcacac tcccaaatct
     6061 gccattgagt tctctgtcta actcacgtgc ccctctccca atcagtagta tgggcatttc
     6121 cccagataat gtccagagtg tgcagttcca aaatggtcgg tgtactctgg atggccgcct
     6181 ggttggcacc accccagttt cattgtcaca tgttgccaag ataagaggga cctctaatgg
     6241 cactgtaatc aaccttactg aattggatgg cacacccttt cacccttttg agggccctgc
     6301 ccccattggg ttcccagacc tcggtggttg tgattggcat atcaatatga cacagtttgg
     6361 ccattctagc cagacccagt atgatgtaga caccacccct gacacttttg tcccccatct
     6421 tggttcaatt caggcaaatg gcattggcag tggtaattat gttggtgttc ttagctggat
     6481 ttccccccca tcacgcccgt ctggctccca agttgacctt tggaagatcc ccaattatgg
     6541 gtcaagtatt acggaggcaa cacatctagc cccttctgta tacccccctg gtttcggaga
     6601 ggtattggtc tttttcatgt caaaaatgcc aggtcctggt gcttataatt tgccctgtct
     6661 attaccacaa gagtacattt cacatcttgc tagtgaacaa gcccctactg taggtgaggc
     6721 tgccctgctc cactatgttg accctgatac cggtcggaat cttggggaat tcaaagcata
     6781 ccctgatggt ttcctcactt gtgtccccaa tggggctagc tcgggtccac aacagctgcc
     6841 gatcaatggg gtctttgtct ttgtttcatg ggtgtccaga ttttatcaat taaagcctgt
     6901 gggaactgcc agctcggcaa gaggtaggct tggtctgcgc cgataatggc ccaagccata
     6961 attggtgcaa ttgctgcttc cacagcaggt agtgctctgg gagcgggcat acaggttggt
     7021 ggcgaagcgg ccctccaaag ccaaaggtat caacaaaatt tgcaactgca agaaaattct
     7081 tttaaacatg acagggaaat gattgggtat caggttgaag cttcaaatca attattggct
     7141 aaaaatttgg caactagata ttcactcctc cgtgctgggg gtttgaccag tgctgatgca
     7201 gcaagatctg tggcaggagt tccagtcacc cgcattgtag attggaatgg cgtgagagtg
     7261 tctgctcccg agtcctctgc taccacattg agatccggtg gcttcatgtc agttcccata
     7321 ccatttgcct ctaagcaaaa acagattcaa tcatctggta ttagtaatcc aaattattcc
     7381 ccttcatcca tttctcgaac cactagttgg gtcgagtcac aaaactcatc gagatttgga
     7441 aatctttctc cataccacgc ggaggctctc aatacagtgt ggttgactcc acccggttca
     7501 acagcctctt ctacactgtc ttctgtgcca cgtggttatt tcaatacaga caggttgcca
     7561 ttattcgcaa ataataggcg atgatgttgt aatatgaaat
//
