#!/usr/bin/env bash

echo "> run ftsZ test"

########################################
##      Setup working directory       ## 
########################################
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory
if [  ! "$SCRATCH"  ]; then
  SCRATCH=/tmp
fi
# Caution: always use a defined working directory:
#          it'll be deleted by the end of the script!
if [  ! "$CB_LOG_FOLDER"  ]; then
  CB_LOG_FOLDER=$SCRATCH/extractGene
fi
mkdir -p "$CB_LOG_FOLDER"

# TMP is used by Python script to create there a log file
export TMP=$CB_LOG_FOLDER

########################################
##      Load Biopython environment    ##
########################################
BIOPYTHON_ENV="/appli/bioinfo/biopython/1.78/env.sh"
if [ -f "$BIOPYTHON_ENV" ]; then
  . $BIOPYTHON_ENV
fi

########################################
##      Configure script for test 1   ##
########################################
GENESNAMES="FTSZ"
TYPE="simple"
GB_FILE="$SCRIPT_DIR/ftsZ-9references.gb"
FNA_FILE="$SCRIPT_DIR/ftsZ-9references.fna"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}.fasta"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type "$TYPE" \
  --genes-names "$GENESNAMES" \
  --gb-file     "$GB_FILE" \
  --output      "$OUT_FILE"  \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed. Log file: ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $FNA_FILE > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created FASTA file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 1: OK"

########################################
##      Configure script for test 2   ##
########################################
TAXO_INC="Corynebacteriaceae, Porphyromonadaceae"
FNA_FILE="$SCRIPT_DIR/ftsZ-9references-tax.fna"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}-tax.fasta"
# During Test 1, we did not pass in a Feature Filter.
# Default filter is CDS:gene, but during Test 2 we check
# that this feature is working fine.
# What does it mean? While scanning Genbank entries, we 
# look for features of type 'CDS' and we check that 'gene'
# qualifier value matches one of $GENESNAMES. 
FEAT_FILTER="CDS:gene"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type         "$TYPE" \
  --genes-names         "$GENESNAMES" \
  --gb-file             "$GB_FILE" \
  --taxo-filter-include "$TAXO_INC" \
  --output              "$OUT_FILE"  \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed. Log file: ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $FNA_FILE > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created FASTA file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi

echo "Test 2: OK"


