#!/usr/bin/env bash

echo "> run COI test"

# Note: test created to fix bug related to missing
#       db_ref qualifier in some features of genbank entries.
#       April 2021
 
########################################
##      Setup working directory       ## 
########################################
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory
if [  ! "$SCRATCH"  ]; then
  SCRATCH=/tmp
fi
# Caution: always use a defined working directory:
#          it'll be deleted by the end of the script!
if [  ! "$CB_LOG_FOLDER"  ]; then
  CB_LOG_FOLDER=$SCRATCH/extractGene
fi
mkdir -p "$CB_LOG_FOLDER"

# TMP is used by Python script to create there a log file
export TMP=$CB_LOG_FOLDER

########################################
##      Load Biopython environment    ##
########################################
BIOPYTHON_ENV="/appli/bioinfo/biopython/1.78/env.sh"
if [ -f "$BIOPYTHON_ENV" ]; then
  . $BIOPYTHON_ENV
fi

########################################
##      Configure script for test 1   ##
########################################
# We are looking for 12S genes in feature type rRNA. 
# Scan qualifier types gene and product.
GENESNAMES="(COI|COX1|CO1|COXI)"
# Example of featFilter: rRNA:gene|product
# which means we want to match features of type rRNA for which
# either 'product' or 'gene' qualifier value match one of
# $GENESNAME.
FEAT_FILTER="CDS:gene"
#Format of Fasta file
TYPE="fasta"
# Reference file to scan
GB_FILE="$SCRIPT_DIR/gbvrt254-COI-partial.seq"
# reference result to compare with
FNA_FILE="$SCRIPT_DIR/gbvrt254-COI-partial.fna"
# Fasta file to create
FNAME="COI"
OUT_FILE="$CB_LOG_FOLDER/${FNAME}.fasta"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --include-regexp "$GENESNAMES" \
  --feature-filter "$FEAT_FILTER" \
  --gb-file        "$GB_FILE" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1 

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed. Log file: ${OUT_FILE}.log" 
  exit 1
fi

diff $OUT_FILE $FNA_FILE > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created FASTA file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 1: OK"


