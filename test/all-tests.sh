#!/usr/bin/env bash

SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

${SCRIPT_DIR}/test-12S.sh

${SCRIPT_DIR}/test-COI.sh

${SCRIPT_DIR}/test-ftsZ.sh

${SCRIPT_DIR}/test-gb.sh

${SCRIPT_DIR}/test-misc.sh

${SCRIPT_DIR}/test-orf2.sh

${SCRIPT_DIR}/test-taxo.sh