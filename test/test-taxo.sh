#!/usr/bin/env bash

echo "> run dump taxonomy test"

########################################
##      Setup working directory       ## 
########################################
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory
if [  ! "$SCRATCH"  ]; then
  SCRATCH=/tmp
fi
# Caution: always use a defined working directory:
#          it'll be deleted by the end of the script!
if [  ! "$CB_LOG_FOLDER"  ]; then
  CB_LOG_FOLDER=$SCRATCH/extractGene
fi
mkdir -p "$CB_LOG_FOLDER"

# TMP is used by Python script to create there a log file
export TMP=$CB_LOG_FOLDER

########################################
##      Load Biopython environment    ##
########################################
BIOPYTHON_ENV="/appli/bioinfo/biopython/1.78/env.sh"
if [ -f "$BIOPYTHON_ENV" ]; then
  . $BIOPYTHON_ENV
fi

########################################
##      Configure script for test 1   ##
########################################
TYPE="1"
GB_FILE="$SCRIPT_DIR/12S-7references.gb"
GB_FILE_REF="$SCRIPT_DIR/12S-7references-tax-3.tsv"
OUT_FILE="$CB_LOG_FOLDER/12S-7references-tax.tsv"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractTaxonomy.py \
  --gb-file        "$GB_FILE" \
  --output         "$OUT_FILE" \
  --output-type    "$TYPE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_FILE_REF > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 1: OK"



########################################
##      Configure script for test 2   ##
########################################

# Special test requiring ETE DBSqlite databank.
ETE_DB_PATH="$CB_LOG_FOLDER/ncbi-tax"
ETE_DB="$ETE_DB_PATH/taxa.sqlite"
if [ ! -e "$ETE_DB" ]; then
  echo "ETE-based taxonomy DB file note found: $ETE_DB"
  echo "Installing it ..."
  mkdir -p $ETE_DB_PATH
  #enter writable directory
  cd $ETE_DB_PATH
  $SCRIPT_DIR/../setup-ete-db.py --db-file $ETE_DB
  RET=$?
  if [  "$RET" -ne 0  ]; then
    echo "/!\ Test has failed: unable to prepare ETE database"
    exit 1
  fi
  cd -
fi

TYPE="taxonomy"
GB_FILE="$SCRIPT_DIR/gbvrt56-reference.gb"
GB_FILE_REF="$SCRIPT_DIR/gbvrt56-reference-taxpath.fas"
OUT_FILE="$CB_LOG_FOLDER/gbvrt56-reference-tax.fas"
INCLUDE_RE="12S"
FEAT_FILTER="rRNA:gene|product"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --gb-file        "$GB_FILE" \
  --include-regexp "$INCLUDE_RE" \
  --feature-filter "$FEAT_FILTER" \
  --output         "$OUT_FILE" \
  --output-type    "$TYPE" \
  --ete-db-file    "$ETE_DB" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $GB_FILE_REF > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created GB file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 2: OK"
