#!/usr/bin/env bash

echo "> run 12S test"

########################################
##      Setup working directory       ## 
########################################
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory
if [  ! "$SCRATCH"  ]; then
  SCRATCH=/tmp
fi
# Caution: always use a defined working directory:
#          it'll be deleted by the end of the script!
if [  ! "$CB_LOG_FOLDER"  ]; then
  CB_LOG_FOLDER=$SCRATCH/extractGene
fi
mkdir -p "$CB_LOG_FOLDER"

# TMP is used by Python script to create there a log file
export TMP=$CB_LOG_FOLDER

########################################
##      Load Biopython environment    ##
########################################
BIOPYTHON_ENV="/appli/bioinfo/biopython/1.78/env.sh"
if [ -f "$BIOPYTHON_ENV" ]; then
  . $BIOPYTHON_ENV
fi

########################################
##      Configure script for test 1   ##
########################################
# We are looking for 12S genes in feature type rRNA. 
# Scan qualifier types gene and product.
GENESNAMES="12S"
# Example of featFilter: rRNA:gene|product
# which means we want to match features of type rRNA for which
# either 'product' or 'gene' qualifier value match one of
# $GENESNAME.
FEAT_FILTER="rRNA:gene|product"
#Format of Fasta file
TYPE="simple"
# Reference file to scan
GB_FILE="$SCRIPT_DIR/12S-7references.gb"
# reference result to compare with
FNA_FILE="$SCRIPT_DIR/12S-7references.fna"
# Fasta file to create
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}.fasta"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type    "$TYPE" \
  --genes-names    "$GENESNAMES" \
  --feature-filter "$FEAT_FILTER" \
  --gb-file        "$GB_FILE" \
  --output         "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed. Log file: ${OUT_FILE}.log" 
  exit 1
fi

diff $OUT_FILE $FNA_FILE > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created FASTA file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi  

echo "Test 1: OK"

########################################
##      Configure script for test 2   ##
########################################
GENESNAMES="12S"
TYPE="simple"
FEAT_FILTER="rRNA:gene|product"
# Caution with taxonomy: set class and superclass, we can have both in Genbank entries
#TAXO_FILTER_INC="Chondrichthyes,Actinopteri,Actinopterygii"
TAXO_FILTER_INC="Chondrichthyes"
GB_FILE="$SCRIPT_DIR/12S-7references.gb"
FNA_FILE="$SCRIPT_DIR/12S-7references-tax.fna"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}-tax.fasta"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type         "$TYPE" \
  --genes-names         "$GENESNAMES" \
  --feature-filter      "$FEAT_FILTER" \
  --gb-file             "$GB_FILE" \
  --taxo-filter-include "$TAXO_FILTER_INC" \
  --output              "$OUT_FILE" \
  --log-level      "debug" &> ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed. Log file: ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $FNA_FILE > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created FASTA file is not equal to reference. Log file: ${OUT_FILE}.log" 
  exit 1
fi

echo "Test 2: OK"

########################################
##      Configure script for test 3   ##
########################################
GENESNAMES="12S"
TYPE="simple"
FEAT_FILTER="rRNA:gene|product"
# Caution with taxonomy: set class and superclass, we can have both in Genbank entries
TAXO_FILTER_INC="Chondrichthyes,Actinopteri,Actinopterygii"
GB_FILE="$SCRIPT_DIR/12S-7references.gb"
FNA_FILE="$SCRIPT_DIR/12S-7references-tax-2.fna"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}-tax.fasta"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/../extractGeneFromGenbank.py \
  --output-type         "$TYPE" \
  --genes-names         "$GENESNAMES" \
  --feature-filter      "$FEAT_FILTER" \
  --gb-file             "$GB_FILE" \
  --taxo-filter-include "$TAXO_FILTER_INC" \
  --log-level "debug" \
  --output              "$OUT_FILE" > ${OUT_FILE}.log 2>&1

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ Test has failed: review content of log file in ${OUT_FILE}.log"
  exit 1
fi

diff $OUT_FILE $FNA_FILE > /dev/null
RET=$?
if [  "$RET" -ne 0 ]; then
  echo "/!\ Test has failed: created FASTA file is not equal to reference. Log file: ${OUT_FILE}.log"
  exit 1
fi

echo "Test 3: OK"

