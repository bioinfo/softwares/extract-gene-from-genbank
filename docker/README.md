## Introduction

This is a Dockerfile to package ExtractGenefromGenbank (EGG) software into a Docker image.

It relies on an Alpine Linux system.

## Pull image

You can directly get a release of EGG image from Docker Hub:

```
docker pull gitlab-registry.ifremer.fr/bioinfo/softwares/public-images/egg_vm/egg_vm:2.0.0
docker tag gitlab-registry.ifremer.fr/bioinfo/softwares/public-images/egg_vm/egg_vm:2.0.0 egg_vm:2.0.0 
```

## Build image

Or you can build the image on your own:

```
docker build -f Dockerfile -t egg_vm .
```

It is worth noting that inside the image, `extractGeneFromGenbank.py` is aliased to `egg`, enabling a more convenient way of running the software, e.g.: `docker run .../... egg_vm egg -h`.

## Run container

Here is how you can run EGG tests.

```
docker run --name egg_vm -i -t --rm -v /tmp:/tmp egg_vm /opt/egg/test/all-tests.sh
```

Results will be located in your `/tmp` directory. In case of arrors, please review log file corresponding to failed test(s).

