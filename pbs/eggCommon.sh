#!/usr/bin/env bash

# #############################################################################
# Section 3 
# Prepare working directory
if [ -d "$EGG_DATAWORK" ]; then
  rm -rf $EGG_DATAWORK
  mkdir -p $EGG_DATAWORK
fi

if [ ! -d "$GBFILEDIR" ]; then
  "ERROR: path not found: $GBFILEDIR"
  exit 1
fi

# #############################################################################
# Section 4
INST_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
export EGG_INST_DIR=$INST_DIR/..
# Prepare working directory structure
# save a copy of this script
mkdir -p $EGG_DATAWORK/scripts
cp $0 $EGG_DATAWORK/scripts/.
# will be used to save GB filtered files (FASTA or GB flat file format)
export EGG_OUTPUT=$EGG_DATAWORK/seq
mkdir -p $EGG_OUTPUT
# will be used to prepare symlinks to GB
export EGG_INPUT=$EGG_DATAWORK/eggdb
mkdir -p $EGG_INPUT
# will be used to store logs of each array job
export EGG_LOGS=$EGG_DATAWORK/logs
mkdir -p $EGG_LOGS
# in case some tools require to access a TMP storage
export TMP=$EGG_LOGS

# #############################################################################
# Section 5
# set file extension for generated files
if [[ "$EGG_TYPE" == "genbank" ]]; then
  export EGG_FEXT=".gb"
else
  export EGG_FEXT=".fasta"
fi

# #############################################################################
# Section 6
# symbolic link of all genbank files in order to proceed with array job
index=0
for gbp in ${GB_PART//,/ }; do
  for gbfile in $GBFILEDIR/$gbp; do
    index=$(($index +1)) 
    ln -s $gbfile $EGG_INPUT/gb@${index}.seq
  done
done

# #############################################################################
# Section 7
# get GB release date; only valid if GB is managed using BeeDeeM bank manager
BANKNAME=$(basename $GBFILEDIR)
BANK=$BANKNAME
if [ -f "${GBFILEDIR}/release-date.txt" ]; then
  while read -r line
    do
      if [[ $line = *"release.time.stamp"* ]]; then
        BANKRELEASE=`echo $line  | awk -F '=' '{print $2}' | awk -F ',' '{print $1}'`
        BANK=${BANKNAME}_${BANKRELEASE}
      fi
    done < ${GBFILEDIR}/release-date.txt
fi
export BANK

# #############################################################################
# Section 8 - adapt to your scheduler
# run egg for each input genbank file
EGG=""
if [[ $index -eq 1 ]]; then
  EGG=$(qsub -o $EGG_LOGS -V $INST_DIR/eggArray.pbs)
else
  EGG=$(qsub -o $EGG_LOGS -V -J 1-${index} $INST_DIR/eggArray.pbs)
fi
echo "EGGArray job number:$EGG"

# once egg jobs are over, merge results and clean
MERGING=$(qsub -o $EGG_LOGS -W depend=afterok:$EGG -V $INST_DIR/eggMerge.pbs)
echo "EGGMerge job number:$MERGING"

