## What for?

Scripts to distribute Genbank filtering process on a cluster.

Start by reading and updating script `egg.sh`.

`eggArray.pbs` and `eggMerge.pbs` are called from `egg.sh`; you should not have to modify them.

Working examples: 
- `eggFtsZ.sh`: retain Genbank entries annotated with FtsZ gene
- `egg12S.sh`: retain Genbank Fish entries annotated with rRNA 12S gene
- `eggCOI.sh`: retain Genbank entries annotated with COI,CO1,COXI,COX1 genes

