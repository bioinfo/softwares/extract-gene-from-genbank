#!/usr/bin/env bash

# This script provides a convenient way to filter out Genbank files
# in a distributed way using jobs array.

# This script will do the following steps:
#  1. setup a working directory and prepare GB file filtering
#  2. submit a first job to filter out GB files through multiple jobs
#  3. submit a second job to merge filtered files into a single one.
#     This job waits for previous filtering job to finish before starting.

# To apply that script framework to your data, update:
#   - section 1: filtering parameters
#   - section 2: path to Genbank databank (GB flat files format)
#   - section 8: qsub commands to adapt to your scheduler

# #############################################################################
# Section 1 - adapt to your needs
# Set feature type to use to filter out GB entries
export EGG_GENESNAMES="COI,COX1,CO1,COXI"
# EGG_TYPE: one of: simple, genbank (simple means FASTA)
export EGG_TYPE="simple"
export EGG_FEAT_FILTER="CDS:gene"
export EGG_TAXO_FILTER_INC="Acanthocephala,Acoela,Acoelomorpha,Annelida,Anthozoa,Arthropoda,Asteroidea,Asterozoa,Bilateria,Brachiopoda,Bryozoa,Calcarea,Cephalochordata,Chaetognatha,Chordata,Cnidariaincertae,Cnidaria,Craniata,Crinoidea,Ctenophora,Cubozoa,Cycliophora,Demospongiae,Deuterostomia,Ecdysozoa,Echinodermata,Echinoidea,Echinozoa,Eleutherozoa,Enteropneusta,Entoprocta,Eumetazoa,Gastrotricha,Gnathifera,Gnathostomulida,Hemichordata,Hexactinellida,Holothuroidea,Homoscleromorpha,Hydrozoa,Kamptozoa,Kinorhyncha,Loricifera,Lophotrochozoa,Mesozoa,Micrognathozoa,Mollusca,Myxozoa,Nematoda,Nematomorpha,Nemertea,Nemertodermatida,Nuda,Onychophora,Ophiuroidea,Orthonectida,Panarthropoda,Pelmatozoa,Phoronida,Placozoa,Platyhelminthes,Priapulida,Polyplacotoma,Polypodiozoa,Porifera,Protostomia,Pterobranchia,Rhombozoa,Rotifera,Scalidophora,Scyphozoa,Sipuncula,Spiralia,Staurozoa,Tardigrada,Tentaculata,Trichoplacidae,Tunicata,Vertebrata,Xenacoelomorpha,Xenoturbellida"

# #############################################################################
# Section 2 - adapt to your needs
# Set path to Genbank 
export GBFILEDIR="/home/ref-bioinfo/ifremer/sebimer/generic-reference-banks/n/Genbank_CoreNucleotide/current/Genbank_CoreNucleotide"

# Set genbank files to retain for filtering
export GB_PART="gbpri*.seq,gbrod*.seq,gbvrt*.seq,gbmam*.seq,gbinv*.seq"

# Set working directory to prepare data
# Caution: $EGG_DATAWORK will be deleted to be refreshed!
# *** Working directory
if [  ! "$EGG_DATAWORK"  ]; then
  export EGG_DATAWORK=$SCRATCH/EGG_COI_Animals_Abyss
fi
# Result file
if [  ! "$EGG_OUTPUT_FILE"  ]; then
  export EGG_OUTPUT_FILE=${EGG_DATAWORK}/coi_animals_abyss.fasta
fi

INST_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

. $INST_DIR/eggCommon.sh

