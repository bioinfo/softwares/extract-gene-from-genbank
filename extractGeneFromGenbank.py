#!/usr/bin/env python
# -*- coding: utf-8 -*-

# #############################################################################
#
# Extract gene specific sequences from genbank files. Well, this was the original
# project, but now this code is capable of extracting features matching more extend
# constrains, i.e. --genes-names argument now accepts a comma separated list of
# (pyhon based) regular expressions. Such expressions are applied on features
# identified by --feature-filter argument. 
#
# For backward compatibility reason argument --genes-names has not been renamed.
#
# How does it work ?
#  1. scan Genbank flat files
#  2. retain entries having feature/qualifier matching "--feature-filter" and
#     "--genes-names"
#  3. extract sequence of that feature and write it on file using
#     either FASTA or Genbank flat file. 
#
# Basic command:
#   python3 extractGeneFromGenbank.py --output-type genbank --genes-names 12S --gb-file ./test/gbvrt56-reference.gb --feature-filter rRNA:gene\|product --output ./test/12S.gb --log-level debug
#
# See ./test directory for more examples.
#
# (c) 2018-2023 Ifremer Bioinformatics Core facility Team (SeBiMER)
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License.
# #############################################################################

from Bio import SeqIO, SeqFeature
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from ete3 import NCBITaxa
import sys, os, re, argparse, logging, codecs, csv

# This is used to construct canonical taxonomy path ('--output-type taxonomy' only).
# During tests using NCBI Taxonomy, it appers that class may be missing in tax path...
#  in such a case, we move up to use superclass... don't know whether it's a good idea, 
#  but at least we have canonical tax pathes to play with!
# TAX_CANONICAL_LINEAGE arrays must have same size.
TAX_CANONICAL_LINEAGE=['superkingdom','kingdom','phylum','class|superclass','order','family','genus','species']
TAX_CANONICAL_LINEAGE_TAG=['d','k','p','c','o','f','g','s']

# Count sequences handled during Genbank file processing
class EggCounter:
  def __init__(self, seqCounter, seqTotal):
    self.seqCounter = seqCounter
    self.seqTotal = seqTotal

# Data processing data block
class EggData:
	def __init__(self, logLevel, logType, logFile, gbfile, fasta, outtype, genenames, excludeRE, addfasta, addtaxo, taxoInc, featFilterDict, dataFile, dataDescMode, eteDbFile):
		self.logLevel=logLevel
		self.logType=logType
		self.logFile=logFile
		self.gbFile=gbfile
		self.outFile=fasta
		self.outType=outtype
		self.featureFilter=featFilterDict
		self.taxoFilter=taxoInc
		self.includeRe=genenames
		self.excludeRe=excludeRE
		self.addTaxo=addtaxo
		self.addFasta=addfasta
		self.dataDescFile=dataFile
		self.dataDescMode=dataDescMode
		self.eteDbFile=eteDbFile

# Command line arguments
def handleArgs() :
	# Arguments definition
	parser = argparse.ArgumentParser("Extract genes from Genbank file")
	parser.add_argument("--gb-file",
						dest="gbfile",
						help="Path to Genbank file to parse.",
						metavar="FILE",
						required=True)
	parser.add_argument("--output",
						dest="fasta",
						help="Path to output file. Will be created or overridden if already exists.",
						metavar="FILE",
						required=False)
	parser.add_argument("--output-type",
						dest="outtype",
						choices=["simple", "taxonomy", "beedeem", "genbank", "fasta"],
						help="Choose output type format. 'simple' is now deprecated, please use 'fasta' instead.",
						required=False,
						default="fasta")
	parser.add_argument("--genes-names",
						dest="genenames",
						help="Gene or genes' names to look for. DEPRECATED, please use --include-regexp",
						metavar="GENE1 or GENE1,GENE2,GENE3",
						required=False)
	parser.add_argument("--include-regexp",
						dest="includeRegExp",
						help="Comma separated list of regular expressions used to locate feature of interest. Replace former --genes-names.",
						metavar="RE1 or RE1,RE2,RE3",
						required=False)
	parser.add_argument("--exclude-regexp",
						dest="excludeRegExp",
						help="Comma separated list of regular expressions used to discard features matched to by include-regexp.",
						metavar="RE1 or RE1,RE2,RE3",
						required=False)
	parser.add_argument("--additionnal-fasta-files",
						dest="addfasta",
						help="Additionnal fasta files provided by user",
						metavar="file1.fasta or file1.fasta,file2.fasta",
						required=False)
	parser.add_argument("--additionnal-files-taxo",
						dest="addtaxo",
						help="Taxonomy to set for additionnal sequences provided by user",
						metavar="ex : Bacteria;Proteobacteria;Gammaproteobacteria;Vibrionales;Vibrionaceae;Vibrio -> Added organism will be sequence id",
						required=False)
	parser.add_argument("--taxo-filter-include",
						dest="taxoInc",
						help="Taxonomy filter: list of phylum names to retain in output",
						metavar="ex : Vibrionales or Actinopteri,Chondrichthyes",
						required=False)
	# Example: featFilter can be something like:  rRNA:gene|product
	# which means we want to match features of type rRNA for which
	# either 'product' or 'gene' qualifier value match one of
	# include-regexp exressions. 
	parser.add_argument("--feature-filter",
						dest="featFilter",
						help="Feature filter: list of feature and qualifier types to use as filter. Default is CDS:gene.",
						metavar="ex : CDS:gene or CDS:gene,rRNA:gene or rRNA:gene|product",
						required=False)
	parser.add_argument("--log-level",
						dest="loglevel",
						choices=["debug", "info"],
						help="Log level. Default is info.",
						required=False,
						default="info")
	parser.add_argument("--log-type",
						dest="logtype",
						choices=["console", "file"],
						help="Log type. Default is console. When using file, expect to define a value to --log-file argument.",
						required=False,
						default="console")
	parser.add_argument("--log-file",
						dest="logfile",
						help="Path to log file. Only used when --log-type is set to file. Please consider using absolute path to log file name.",
						required=False)
	parser.add_argument("--data-file",
						dest="datafile",
						help="Path to a data file. CSV file containing additional information that will be introduced in output file. First row must contain header with first column being 'ID' string (without any quotes). Remaining column names can be any string.",
						required=False)
	parser.add_argument("--data-desc-mode",
						dest="datadescmode",
						help="From data file (--data-file), if any, a one-line description is created. In turn that description can either replace Genbank description, be inserted before or after that description, or not created at all.",
						metavar="Use one of: replace, before, after, none (default).",
						required=False,
						default="none")
	parser.add_argument("--ete-db-file",
						dest="etedbfile",
						help="Path to ETE3 sqlite database file. Argument mandatory if using 'taxonomy' output type.",
						required=False)

	# Arguments initialization  
	args = parser.parse_args()
	gbfile=args.gbfile
	fasta=args.fasta
	outtype=args.outtype
	logLevel=args.loglevel
	logType=args.logtype
	logFile=args.logfile
	dataFile=args.datafile
	dataDescMode=args.datadescmode
	eteDbFile=args.etedbfile
	genenames=None
	excludeRE=None
	# Name of genes to match in input Genbank file... now deprecated, use includeRegExp.
	if args.genenames is not None:
		#No logging here, not yet initialized
		print('--genes-names argument is deprecated. Please use --include-regexp instead.')
		genenames=list(map(str.strip,args.genenames.lower().split(",")))
	# No, else: --include-regexp overrides deprecated --genes-names
	if args.includeRegExp is not None:
		genenames=list(map(str.strip,args.includeRegExp.lower().split(",")))
	if genenames is None:
		raise ValueError('No matching expressions provided. Please use --include-regexp argument.')
	if 'taxonomy' in outtype and eteDbFile is None:
		raise ValueError('"taxonomy" output type requires ETE sqlite database file". Please use --ete-db-file argument.')
	if args.excludeRegExp is not None:
		excludeRE=list(map(str.strip,args.excludeRegExp.lower().split(",")))
	# Fasta file containing additional sequences to add (FASTA output only)
	if args.addfasta :
		addfasta=list(map(str,(args.addfasta).split(",")))
	else :
		addfasta=None
	# Handle Taxonomy to introduce in output file
	if args.addtaxo is not None :
		addtaxo=args.addtaxo
	else :
		addtaxo=None
	# Taxonomy filter to retain only entries matching that filter
	if args.taxoInc :
		taxoInc=list(map(str.strip,args.taxoInc.split(",")))
	else :
		taxoInc=None
	# Feature filter
	if args.featFilter :
		featFilter=list(map(str.strip,args.featFilter.split(",")))
		featKeys = []
		qualKeys = []
		for fk in featFilter:
			featKeys.append(fk.split(":")[0].strip())
			qualKeys.append(list(map(str.strip,fk.split(":")[1].strip().split("|"))))
		featFilterDict = dict(zip(featKeys, qualKeys))
	else :
		featFilterDict={'CDS': ['gene']}
	# Handle arguments' errors
	if not os.path.isfile(gbfile):
		parser.error("Genbank file {} does not exist".format(gbfile))
	return EggData(logLevel, logType, logFile, gbfile, fasta, outtype, genenames, excludeRE, addfasta, addtaxo, taxoInc, featFilterDict, dataFile, dataDescMode, eteDbFile)
		
# Main function to seek for a feature into a genbank file
def seekForGene(eCounter, egData, addOnData):
	# if no output specified, write to stdout
	if not egData.outFile :
		output = sys.stdout
	else:
		output = open(egData.outFile, 'w')
		# added the following to fix this error with some Genbank files:
		#  UnicodeDecodeError: 'utf-8' codec can't decode byte 0xae
		handle = codecs.open(egData.gbFile, 'r', encoding='utf-8', errors='ignore')
	# parse input genbank file
	for record in SeqIO.parse(handle, "genbank") : 
	#	taxonID = getTaxonID(record)
		# seek for specified gene in input genbank file
		logging.debug("> Record {}".format(record.name))
		#seekForGeneInRecord(eCounter, record, outtype, output, genenames, excludeRE, taxoInc, featFilterDict, addOnData, dataDescMode, eteDbFile)
		seekForGeneInRecord(eCounter, egData, addOnData, record, output)
	output.close()
	handle.close()

# Seek for a feature in a genbank record and format the output
def seekForGeneInRecord(eCounter, egData, addOnData, record, output):
	eCounter.seqTotal += 1
	# filter by taxonomy if requested
	if egData.taxoFilter is not None:
		retainRecord=False
		taxonomy=record.annotations['taxonomy']
		if taxonomy is not None:
			for phylum in egData.taxoFilter:
				if phylum in taxonomy:
					logging.debug("  - Phylum ok: {}".format(phylum))
					retainRecord=True
					break
	else:
		retainRecord=True
	if retainRecord == False:
		return None

	# look for keyword CDS and gene in genbank record features
	for feature in record.features:
		# Example: featFilter can be something like:  rRNA:gene|product
		# which means we want to match features of type rRNA for which
		# either 'product' or 'gene' qualifier value match one of
		# genenames
		for featKey, qualKeys in egData.featureFilter.items():
			logging.debug("  - Feature '{}' is '{}'?".format(feature.type, featKey))
			discardFeat=False
			if feature.type == featKey:
				for qualKey in qualKeys:
					logging.debug("    - qualifier '{}' in '{}'?".format(qualKey, list(feature.qualifiers.keys())))
					if qualKey in feature.qualifiers:
						# split qualifier value into parts
						qualParts=feature.qualifiers.get(qualKey)[0]
						# then find whether or not one of these parts match a gene name
						logging.debug("      can find '{}' in '{}'?".format(egData.includeRe, qualParts))
						matchOk=False
						for geneName in egData.includeRe:
							p=re.compile(geneName,re.IGNORECASE)
							m=p.search(qualParts)
							if m!=None:
								logging.debug("        '{}' found in '{}'".format(geneName,qualParts))
								# now, check whether we have to discard match using excludeRE, if any
								matchOk=True
								if egData.excludeRe is None:
									break
								logging.debug("        should discard '{}' if it has '{}'".format(qualParts, egData.excludeRe))
								for exclRE in egData.excludeRe:
									p=re.compile(exclRE,re.IGNORECASE)
									m=p.search(qualParts)
									if m!=None:
										logging.debug("        '{}' found in '{}': ".format(exclRE,qualParts))
										matchOk=False
										discardFeat=True
										break
								if discardFeat:
									break
						if discardFeat:
							break
						if matchOk:
							eCounter.seqCounter+=1
							logging.debug("        {}: got it! {}".format(qualParts,eCounter.seqCounter))
							# get taxon id to find associate taxonomy
							taxonID = getTaxonID(record)
							#formatOutput(eCounter, record, feature, egData.outType, output, taxonID, addOnData, egData.dataDescMode, egData.eteDbFile)
							formatOutput(eCounter, egData, addOnData, record, output, feature, taxonID)
							# when one qualifier has matched, exit from loop otherwise 
							# we possibly get several copies of the same feature sequence
							break

	return None

# Define the description for each sequence, then extract feature sequence and write result
def formatOutput(eCounter, egData, addOnData, record, output, feature, taxonID):
	#sequence
	try:
		sequence = feature.extract(record.seq)
	except ValueError as e :
		logging.debug("Error : {}".format(e))
		sequence = None
	#Problem with sequence, return
	if sequence == None :
		logging.warn("No sequence for feature find in record number {} (feature rejected)".format(record.id))
		return
	# Additional description?
	addOnDataDesc=formatAddonDataDescription(record, addOnData)
	#description	
	if 'beedeem' in egData.outType:
		desc = formatDescriptionBeedeem(record, feature, taxonID, addOnDataDesc, egData.dataDescMode)
	else:
		#classical fasta output file, even for genbank
		desc = formatDescriptionSimpleFasta(record, feature, addOnDataDesc, egData.dataDescMode)	
	#seqid: ensure to have unique ones over the entire processing
	if "protein_id" in feature.qualifiers :
		seqid = feature.qualifiers['protein_id'][0]
	elif "locus_tag" in feature.qualifiers :
		seqid = feature.qualifiers['locus_tag'][0]
	else :
		seqid = record.annotations['accessions'][0]
	idx=seqid.rfind(".")
	if idx!=-1:
		seqid=seqid[:idx]
	seqid="{}_{}".format(seqid, str(eCounter.seqCounter))
	if 'taxonomy' in egData.outType :
		# sequence description is replace by taxon id first, then canonical taxon path
		#dbFile='/Users/pgdurand/devel/ifremer/extract-gene-from-genbank/test/ncbi-tax/taxa.sqlite'
		taxID=taxonID.split(':')[1]
		taxPath=getCanonicalTaxPath(egData.eteDbFile, taxID)
		desc='{}:{}\n'.format(taxID, taxPath)
	#save
	logging.debug("        - save {} as {}".format(seqid, egData.outType))
	if 'genbank' in egData.outType:
		gb_record=record
		featureToKeep=feature
		#Preparation general Genbank record annotations
		annotations={}
		annotations['date']=gb_record.annotations['date']
		annotations['molecule_type']=gb_record.annotations['molecule_type']
		annotations['topology']='linear'
		annotations['data_file_division']=gb_record.annotations['data_file_division']
		annotations['accessions']=gb_record.annotations['accessions']
		annotations['source']=gb_record.annotations['source']
		annotations['organism']=gb_record.annotations['organism']
		annotations['taxonomy']=gb_record.annotations['taxonomy']
		annotations['keywords']=gb_record.annotations['keywords']
		# In addition, we always keep the 'source' feature providede in Genbank entry
		# to keep track of all information about origin of featureCDS
		featureSource=None
		for feature in gb_record.features:
			if feature.type == "source": # we always conserve the source feature
				featureSource=feature
				dataRow=None
				if gb_record.annotations['accessions'][0] in addOnData:
					dataRow=addOnData[gb_record.annotations['accessions'][0]]
				elif gb_record.name in addOnData:
					dataRow=addOnData[gb_record.name]
				if dataRow is not None:
					logging.debug("          upgrade source feature with {}".format(dataRow))
					for key, value in dataRow.items():
						if key != 'ID':
							featureSource.qualifiers[key.lower()]=[value]
				break
		# Do wa have something to save?
		if featureToKeep!=None and featureSource!=None:
			sequence = featureToKeep.extract(gb_record.seq)
			#of note, in code, genbank uses 0-based location values
			location = SeqFeature.FeatureLocation(
				SeqFeature.ExactPosition(0),
				SeqFeature.ExactPosition(len(sequence)),
				featureToKeep.location.strand)
			featureToKeep.location = location
			featureSource.location = location
			
			#print(f'New location: {featureToKeep.location}')
			my_features = [featureSource, featureToKeep]
			
			record = SeqRecord(sequence, id=seqid, name="{}_{}".format(gb_record.name, str(eCounter.seqCounter)), description=desc, annotations=annotations, features = my_features)
			#print(record)  
			SeqIO.write(record, output, 'genbank')
	else:
		outrecord = SeqRecord(Seq(str(sequence)), id=str(seqid), description=desc)
		SeqIO.write(outrecord, output,"fasta")

# Get taxon ID from Genbank record.
def getTaxonID(record):
	for feature in record.features:
		if 'source' in feature.type:
			dbXref = feature.qualifiers.get('db_xref')
			if dbXref is not None:
				i=0
				while i<len(dbXref):
					if dbXref[i] is not None and 'taxon:' in dbXref[i] :
						return dbXref[i]
					i+=1
	return "taxon:0"

# Format a description for any provided addOnData.
def formatAddonDataDescription(gb_record, addOnData):
	if not addOnData:
		return None
	dataRow=None
	descData=None
	if gb_record.annotations['accessions'][0] in addOnData:
		dataRow=addOnData[gb_record.annotations['accessions'][0]]
	elif gb_record.name in addOnData:
		dataRow=addOnData[gb_record.name]
	if dataRow is not None:
		descData=''
		for key, value in dataRow.items():
			if key != 'ID':
				descData+=''.join([key.lower(), '=', value, '; '])
		logging.debug("          upgrade description with {}".format(descData))
	return descData

# Format description for BeeDeeM software.
def formatDescriptionBeedeem(record, featureCDS, taxonID, addOnDataDesc, dataDescMode):
	return "{} [[{}]]".format(formatDescriptionSimpleFasta(record, featureCDS, addOnDataDesc, dataDescMode), taxonID)

# Format description for classical Fasta.
def formatDescriptionSimpleFasta(record, featureCDS, addOnDataDesc, dataDescMode):
	originaccession = record.id
	origindefinition = record.description
	if "product" in featureCDS.qualifiers:
		product = featureCDS.qualifiers['product'][0]
	else :
		product = "feature"
	desc="{} extracted from {}: {}".format(product, originaccession, origindefinition)
	if addOnDataDesc:
		# only consider replace, after and before modes. In all other
		# cases, do not alter desc created above.
		match dataDescMode:
			case 'replace':
				desc=addOnDataDesc
			case 'after':
				desc="{}. {}".format(desc, addOnDataDesc)
			case 'before':
				desc="{}. {}".format(addOnDataDesc, desc)
	return desc

# Format description to get taxonomy
def formatDescriptionWithTaxonomy(record):
	if "organism" in record.annotations:
		organism = record.annotations["organism"]
	else :
		organism = ""
	if "taxonomy" in record.annotations:
		taxo = record.annotations["taxonomy"]
		if taxo[-1:] == ".":
			taxo = taxo[:-1]
			taxonomy = "; ".join(taxo)
		else :
			taxonomy = ""
	return "{}; {}".format(taxonomy,organism)

def getTaxIDGivenRank(dict, search_lin):
	taxID=-1
	for sl in search_lin.split('|'):
		logging.debug("*** {}".format(sl))
		taxID=[tx for tx, lin in dict.items() if lin == sl]
		if len(taxID)==1:
			return taxID[0]
	return -1

# Return a canonical tax path or an empty string if a taxID cannot be found in underlying DB
def getCanonicalTaxPath(dbFile, taxID):
	ncbi = NCBITaxa(dbFile, update=False)
	lineage = ncbi.get_lineage(taxID)
	ranks=ncbi.get_rank(lineage)
	canonical_lineage=[]
	for rank in TAX_CANONICAL_LINEAGE:
		tx=getTaxIDGivenRank(ranks, rank)
		if tx!=-1:
			canonical_lineage.append(tx)
		else:
			logging.debug("          - canonical rank '{}' not found for taxID: {}".format(rank, taxID))
			logging.debug("            available ranks are: '{}'".format(ranks))
	canonicalTaxPath='no canonical taxonomy path found'
	lenRef=len(TAX_CANONICAL_LINEAGE)
	lenCan=len(canonical_lineage)
	logging.debug("          - getting canonical taxIDs '{}' (items: {}/{})".format(canonical_lineage, lenCan, lenRef))
	if lenRef==lenCan:
		names = ncbi.get_taxid_translator(canonical_lineage)
		canonicalTaxPath='root;'
		idx=0
		for taxid in canonical_lineage:
			canonicalTaxPath+='{}__{};'.format(TAX_CANONICAL_LINEAGE_TAG[idx],names[taxid])
			idx+=1
	else:
		logging.debug("            canonical tax path rejected ")
	return canonicalTaxPath
	
# If the user provides its own sequence files to add to genbank extract
def addData(fasta, addfasta, addtaxo):
	if fasta is None:
		output = sys.stdout
	else:
		output = open(fasta, 'w')
	
	for addfile in addfasta:
		handle = open(addfile, "rU")
		for record in SeqIO.parse(handle, "fasta") :
			addrecord=SeqRecord(Seq(record.seq), id=record.id, name=record.name, description="{};{}".format(addtaxo,record.id))
			SeqIO.write(addrecord, output,"fasta")	
		handle.close()	
	output.close()

# Entrypoint of the application
if __name__ == '__main__':
	
	#Handle script arguments
	egData = handleArgs()
	
	# Logging config
	if 'file' in egData.logType:
		logFName=egData.logFile
		if logFName is None:
			logFName='{}/{}.log'.format(os.path.dirname(egData.outFile), os.path.basename(egData.gbFile))
		logging.basicConfig(
			level=(logging.DEBUG if egData.logLevel=="debug" else logging.INFO),
			format='%(asctime)s %(levelname)s %(message)s',
			filename=logFName,
			filemode='w')
	else:
		logging.basicConfig(
			level=(logging.DEBUG if egData.logLevel=="debug" else logging.INFO),
			format='%(asctime)s %(levelname)s %(message)s',
			stream=sys.stdout)
	
	# Display parameters
	logging.info("* START: Extract Gene from Genbank")
	logging.info(" GenBank file to scan   : {}".format(egData.gbFile))
	logging.info(" Features to match      : ")
	for item in egData.featureFilter:
		logging.info("   {}:{}".format(item,egData.featureFilter[item]))
	logging.info(" RegExp to match        : {}".format(egData.includeRe))
	logging.info(" RegExp to discard match: {}".format(egData.excludeRe))
	logging.info(" Addon data file        : {}".format(egData.dataDescFile))
	logging.info(" Addon description mode : {}".format(egData.dataDescMode))
	logging.info(" Taxonomy to include    : {}".format(egData.taxoFilter))
	logging.info(" Result file to create  : {}".format(egData.outFile))
	logging.info(" Result type to create  : {}".format(egData.outType))
	if 'taxonomy' in egData.outType:
		logging.info(" ETE sqlite DB file te  : {}".format(egData.eteDbFile))
	logging.info("---")

	# Handle additional data file
	addOnData={}
	if egData.dataDescFile:
		with open(egData.dataDescFile, newline='') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				addOnData[row['ID']]=row

	eCounter=EggCounter(0, 0)
	seekForGene(eCounter, egData, addOnData)

	if egData.addFasta is not None and egData.addTaxo is not None and egData.outType=="taxonomy":
		addData(egData.outFile, egData.addFasta, egData.addTaxo)
	
	logging.info("Total sequence(s) scanned : {}".format(eCounter.seqTotal))
	logging.info("Total sequence(s) retained: {}".format(eCounter.seqCounter))
	logging.info("* STOP")

