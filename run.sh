#!/usr/bin/env bash

#############################################################
## This script shows how to use 'extractGeneFromGenbank.py'.
#############################################################


########################################
##      Setup working directory       ## 
########################################
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )

# *** Working directory
if [  ! "$SCRATCH"  ]; then
  SCRATCH=/tmp
fi
# Caution: always use a defined working directory
if [  ! "$CB_LOG_FOLDER"  ]; then
  CB_LOG_FOLDER=$SCRATCH/extractGene
fi
mkdir -p "$CB_LOG_FOLDER"

# TMP is used by Python script to create there a log file
export TMP=$CB_LOG_FOLDER

########################################
##      Load Biopython environment    ##
########################################
BIOPYTHON_ENV="/appli/bioinfo/biopython/1.76/env.sh"
if [ -f "$BIOPYTHON_ENV" ]; then
  . $BIOPYTHON_ENV
fi

########################################
##      Setup your parameters here    ##
########################################
GENESNAMES="12S"
FEAT_FILTER="rRNA:gene|product"
TYPE="simple"
TAXO_FILTER_INC="Lepidosauria"
GB_FILE="$SCRIPT_DIR/test/gbvrt56-reference.gb"
OUT_FILE="$CB_LOG_FOLDER/${GENESNAMES}.fna"

########################################
##      Execute script                ##
########################################
$SCRIPT_DIR/extractGeneFromGenbank.py \
  --include-regexp "$GENESNAMES" \
  --gb-file        "$GB_FILE" \
  --feature-filter "$FEAT_FILTER" \
  --taxo-filter-include "$TAXO_FILTER_INC" \
  --output-type    "$TYPE" \
  --output         "$OUT_FILE" 

RET=$?
if [  "$RET" -ne 0  ]; then
  echo "/!\ FAILED: review content of log file in $TMP"
  exit 1
fi

