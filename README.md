# Extract Genes from Genbank

A python code to extract features from Genbank entries.

## Dependencies

* Python 3
* Biopython 1.78
* ETE Taxonomy Toolkit v3 (and its dependency: six)

## Install and test

### Docker installation and use

[Review this documentation](./docker/README.md).

### Manual installation using Conda

First, prepare environment:

```
#Setup a conda environement using conda create .../..., then
conda install biopython=1.78 ete3
```

Then:
```
git clone https://gitlab.ifremer.fr/bioinfo/softwares/extract-gene-from-genbank.git 
cd extract-gene-from-genbank/test
./all-tests.sh
```

Test may fail if Biopython or ete3 is not available.

### How to run

copy/paste the example script run.sh and modify:
```
#gene or genes names to look for (comma separared if several genes)
GENESNAMES="FTSZ"
# type of feature to use to filter GB entries; set to emty string for default (CDS:product)
FEAT_FILTER="rRNA:gene|product"
#type of output file : genbank, simple (genbank id and description), beedeem (with taxon id) or taxonomy (full taxonomy extracted with beedeem)
TYPE="taxonomy"
#Taxonomy filter (set to empty string to avoid taxonomy filtering)
TAXO_FILTER_INC="Lepidosauria"
```

Then run the script:
```
./run.sh
```
