#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from ete3 import NCBITaxa

# ETE Toolkit v3. 
# https://github.com/etetoolkit/ete/tree/3.1.3/ete3

# A simple code to prepare ETE DB file to be used by EGG software ('--output-type taxonomy' option).

def handleArgs() :
	#Arguments definition
	parser = argparse.ArgumentParser("Prepare ETE3 database from NCBI taxonomy data")
	parser.add_argument("--db-file",
						dest="dbfile",
						help="Path to 'taxa.sqlite' file to create.",
						metavar="FILE",
						required=True)
	parser.add_argument("--tax-file",
						dest="taxfile",
						help="Path to local 'taxdump.tar.gz'. If not provided, will be downloaded.",
						metavar="FILE",
						required=False)
	args = parser.parse_args()
	dbfile=args.dbfile
	taxfile=args.taxfile
	return dbfile, taxfile

if __name__ == '__main__':
    # To do once: preparing ETE sqlite database from NCBI Taxonomy tar.gz file
    #  (wget http://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz)
    
    dbfile, taxfile = handleArgs()
    ncbi = NCBITaxa(dbfile=dbfile, taxdump_file=taxfile, update=True)
    
