#!/usr/bin/env python
# -*- coding: utf-8 -*-

# #############################################################################
#
# Extract taxonomy from genbank files. 
#
# How does it work ?
#  1. scan Genbank flat files
#  2. retain entries having feature 'source' with db_xref:taxon
#  3. dump data into a text file into suitable formats 
#
# Basic command:
#   python3 extractTaxonomy.py --output-type 1 --gb-file ./test/12S-7references.gb --output ./test/tax-dump.tsv
#
# See ./test directory for more examples.
#
# (c) 2023 Ifremer Bioinformatics Core facility Team (SeBiMER)
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License.
# #############################################################################

from Bio import SeqIO
import sys, os, argparse, logging, codecs

global seqCounter, seqTotal

def handleArgs() :
	#Arguments definition
	parser = argparse.ArgumentParser("Extract taxonomy from Genbank file")
	parser.add_argument("--gb-file",
						dest="gbfile",
						help="Genbank file to parse",
						metavar="FILE",
						required=True)
	parser.add_argument("--output",
						dest="outfile",
						help="Output taxonomy file. Text tabulated file.",
						metavar="FILE",
						required=True)
	parser.add_argument("--output-type",
						dest="outtype",
						choices=["1", "2", "3", "4", "5"],
						help="Choose output format",
                        metavar="1: full tax path and taxID ; 2: tax path without organism, but with taxh ID ; 3: qiime with full tax path ; 4: qiime with partial tax path (no organism) ; 5: qiime with taxID only",
						required=False,
						default="taxid")
	parser.add_argument("--log-level",
						dest="loglevel",
						choices=["debug", "info"],
						help="Log level. Default is info.",
						required=False,
						default="info")
	parser.add_argument("--log-type",
						dest="logtype",
						choices=["console", "file"],
						help="Log type. Default is console. When using file, expect to define a value to --log-file argument.",
						required=False,
						default="console")
	parser.add_argument("--log-file",
						dest="logfile",
						help="Path to log file. Only used when --log-type is set to file. Please consider using absolute path to log file name.",
						required=False)
	#Arguments initialization  
	args = parser.parse_args()
	gbfile=args.gbfile
	outfile=args.outfile
	outtype=args.outtype
	logLevel=args.loglevel
	logType=args.logtype
	logFile=args.logfile
	# Handle arguments' errors
	if not os.path.isfile(gbfile):
		parser.error("Genbank file {} does not exist".format(gbfile))
	return logLevel, logType, logFile, gbfile, outfile, outtype

# Get the taxon id for this entry
def getTaxonID(record):
    taxID=None
    for feature in record.features:
        if feature.type == "source":
            logging.debug("  - Feature 'source' found")
            for key, value in feature.qualifiers.items():
                if key == "db_xref" and "taxon" in value[0]:
                    logging.debug("    - qualifier db_xref found with {}:{}".format(key, value))
                    taxID=value[0].split(':')[1]
                # No else! By design, norodb_genotype overrides taxID from NCBI
                if key == "norodb_genotype":
                    logging.debug("    - qualifier norodb_genotype found with {}:{}".format(key, value))
                    taxID=value[0]
    return taxID

# Format description to get taxonomy
def formatDescriptionWithTaxonomy(record, outtype):
    if "organism" in record.annotations:
        organism = record.annotations["organism"]
        logging.debug("  - Organism is '{}'".format(organism))
    else :
        organism = ""
    if "taxonomy" in record.annotations:
        logging.debug("  - Taxonomy is '{}'".format(record.annotations["taxonomy"]))
        taxonomy = ";".join(record.annotations["taxonomy"])
    else :
        taxonomy = ""
    taxPath=''
    match outtype:
        case "2"|"4":
            taxPath=taxonomy
        case _:
            taxPath="{};{}".format(taxonomy,organism)
    logging.debug("    - retain taxpath is '{}'".format(taxPath))
    return taxPath

# Dump a text file with taxonomy information
def dumpTaxonomyFile(record, outStream, outtype):
    seqID=record.annotations['accessions'][0]
    taxPath=formatDescriptionWithTaxonomy(record, outtype)
    taxID=getTaxonID(record)
    match outtype:
        case "1"|"2": # Tax path with organism (1) or without (2)
            outStream.write(f'{seqID}\t{taxID}\t{taxPath}\n')
        case "3"|"4": # QIIME and Tax path with organism (3) or without (4)
            outStream.write(f'{seqID}\t{taxPath}\n')
        case "5": # QIIME and Tax path with taxID only
            outStream.write(f'{seqID}\t{taxID}\n')
        case _:
            outStream.write(f'{seqID}\t{taxID}\t{taxPath}\n')

#main function to seek for a gene into a genbank file
def seekForTaxonomy(gbfile, outfile, outtype):
    output = open(outfile, 'w')
    # added the following to fix this error with some Genbank files:
    #  UnicodeDecodeError: 'utf-8' codec can't decode byte 0xae
    handle = codecs.open(gbfile, 'r', encoding='utf-8', errors='ignore')
	# parse input genbank file
    for record in SeqIO.parse(handle, "genbank") : 
        logging.debug("> Record {}".format(record.name))
        dumpTaxonomyFile(record, output, outtype)
    output.close()
    handle.close()	


if __name__ == '__main__':
	
	#Handle script arguments
	logLevel, logType, logFile, gbfile, outfile, outtype = handleArgs()
	
	#logging config
	if 'file' in logType:
		logFName=logFile
		if logFName is None:
			logFName='{}/{}.log'.format(os.path.dirname(outfile), os.path.basename(gbfile))
		logging.basicConfig(
			level=(logging.DEBUG if logLevel=="debug" else logging.INFO),
			format='%(asctime)s %(levelname)s %(message)s',
			filename=logFName,
			filemode='w')
	else:
		logging.basicConfig(
			level=(logging.DEBUG if logLevel=="debug" else logging.INFO),
			format='%(asctime)s %(levelname)s %(message)s',
			stream=sys.stdout)
	#display parameters
	logging.info("* START: Extract Taxonomy from Genbank")
	logging.info(" GenBank file to scan   : {}".format(gbfile))
	logging.info(" Result file to create  : {}".format(outfile))
	logging.info(" Result type to create  : {}".format(outtype))
	logging.info("---")

	seekForTaxonomy(gbfile, outfile, outtype)
	
	logging.info("* STOP")

